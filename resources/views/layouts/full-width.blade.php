<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        @if(View::hasSection("title"))
            @yield('title')
        @else
            <?php echo isset($title) ? $title : setting('site.title');?>
        @endif
    </title>

    <link rel="stylesheet" type="text/css"
          href="{{asset("assets/css/corev15.css")}}">


    {{-- BOOTSTRAP and FONT AWESOME --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="{{asset("assets/css/bootstrap.css")}}"
          rel="stylesheet">
    <link href="{{asset("assets/css/main.css")}}"
          rel="stylesheet">
    <link href="{{asset("assets/css/sidebar-menu.css")}}"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

    <meta name="site_url" content="{{route("home")}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <script src="{{asset("assets/js/jquery.js")}}"
            type="text/javascript"></script>
    @if(View::hasSection('meta'))
        @yield("meta")
    @else
        @include('components.metas.default')
    @endif

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    {{-- SITE STYLE --}}
    <link rel="stylesheet" href="{{asset("assets/css/custom.css")}}">

    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    @yield("css")
    @stack("css-code")

</head>
<body class="bg-body ms-backgroundImage">
@include("components.elements.notification")
<div id="s4-workspace" style="height: 100%;">
    @include("layouts.header")
    <div class="<?php echo isset($class) ? $class : ""; ?>">
        @include('components.home.side-banner')

        <div class="container none-padding bg-f">

            <div class="col-xs-12 none-padding content">
                <!-- content -->
                <div data-name="ContentPlaceHolderMain">
                    <div class="col-xs-12 none-padding mar-top-15">
                        <div class="col-xs-12 none-padding" style="min-height: 500px">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <footer>
        @include("layouts.footer")
    </footer>
</div>

<script type="text/javascript"
        src="{{asset("assets/js/initstrings.js")}}"></script>
<script type="text/javascript"
        src="{{asset("assets/js/init.js")}}"></script>

<script type="text/javascript"
        src="{{asset("assets/js/blank.js")}}"></script>

<script type="text/javascript"
        src="{{asset("assets/js/strings.js")}}"></script>
{{--<script type="text/javascript"
        src="{{asset("assets/js/sp.js")}}"></script>--}}
<script type="text/javascript"
        src="{{asset("assets/js/core.js")}}"></script>
<script type="text/javascript"
        src="{{asset("assets/js/mquery.js")}}"></script>
{{--<script type="text/javascript"
        src="{{asset("assets/js/sp_002.js")}}"></script>--}}

<script src="{{asset("assets/js/owl.js")}}"
        type="text/javascript"></script>
<script src="{{asset("assets/js/bootstrap.js")}}"
        type="text/javascript"></script>
<script src="{{asset("assets/js/jquery_003.js")}}"
        type="text/javascript"></script>
<script src="{{asset("assets/js/jquery_002.js")}}"
        type="text/javascript"></script>
<script src="{{asset("assets/js/control.js")}}"
        type="text/javascript"></script>
<script src="{{asset("assets/js/menu.js")}}"
        type="text/javascript"></script>
<script src="{{asset("assets/js/sidebar-menu.js")}}"
        type="text/javascript"></script>
<script src="{{asset("assets/js/blank.js")}}"
        type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
@yield('javascript')
@yield('script')
@stack("js-code")
<script assets="{{asset('assets/js/main.js') }}"></script>
<!-------------------------------------------------------------------------------------?>
*       === Công ty Cổ phần Đầu tư Phát triển IMUS - IMUS Join Stock Company ===        *
*                                                                                       *
*       Địa chỉ:             Số nhà 77, ngõ 46, đường Việt Bắc, TP. Thái Nguyên         *
*       Số điện thoại:      0869 011 011                                                *
*       Email:              contact@imus.vn                                             *
<--------------------------------------------------------------------------------------->
</body>
</html>

