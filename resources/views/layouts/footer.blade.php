<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/
?>
<div class="">
    <div class="bg-footer" style="position: relative">

        <div class="container none-padding ">
            <div class="footer">
                <div class="row" style="margin: 0">
                    <div class="col-md-4">
                        <h3 class="tieude">Giới thiệu</h3>
                        <div class="content">
                                {!! option('site.about') !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3 class="tieude">liên kết web</h3>
                        <div class="content">
                            <div class="list">
                                <ul class="ul-footer">
                                    @foreach(menu("primary","_json") as $item)
                                        <li><a style="color: white" href="{{$item->url}}">{{$item->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fimus.jsc%2F&tabs=timeline&width=340&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=441147446645129" width="340" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
