<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/
?>
<div id="s4-bodyContainer">
    <div class="container-fuild none-padding">
        <div class="col-xs-12 none-padding">
            <!-- head -->
            <div>
                <!-- banner top -->
                <div class="col-xs-12 none-padding">
                    <div class="col-xs-12 none-padding banner-top">
                        <div class="container none-padding">
                            <a href="{{route("home")}}" class="banner">
                                <img src="{{asset("storage/".option('site.header_image'))}}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end banner -->
    {{--            @dd($menus))--}}
    <!-- menu -->
        <div class="col-xs-12 none-padding">
            <div class="container none-padding menu-bg">
                <div class="col-xs-12 none-padding ">
                    <div class="col-xs-10 none-padding banner-right ">
                        <div class="container none-padding">
                            <div class="pushmenu-push ">
                                <div class="pushmenu pushmenu-left menu-open">
                                    <section class="ul-width ">
                                        <ul class="sidebar-menu">
                                            @foreach($menus['primary'] as $item)
                                                @if($loop->index==0)
                                                    <li class="treeview">
                                                        <a href="{{$item->link()}}" class="">
                                                            <img src="{{asset("images/ic_home.png")}}"
                                                                 class="icon-home">
                                                        </a>
                                                    </li>
                                                @else
                                                    <li class="treeview">
                                                        <a target="{{$item->target}}" href="{{$item->link()}}">
                                                            <span>{{$item->title}}</span>
                                                            @if(count($item->children)!=0)
                                                                <span><img src="{{asset("images/ic-dropdown-yellow.svg")}}"></span>
                                                            @endif
                                                        </a>
                                                        @if(count($item->children)!=0)
                                                            <ul class="col-xs-12 none-padding treeview-menu"
                                                                style="display: none;">
                                                                @foreach($item->children as $mItem)
                                                                    <li class="col-xs-12 none-padding">
                                                                        <a target="{{$item->target}}"
                                                                           href="{{$item->link()}}" class="span-head">
                                                                            {{$mItem->title}}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <!--home-mobile  -->
                        <div class="col-xs-6 none-padding icon-menu" id="nav_list">
                    <span id="nav_list1" class="active">

					</span>
                        </div>
                        <!--  -->
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- head end-->
</div>
