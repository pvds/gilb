@extends("layouts.master",[
    "title"=>getTitle($page->title),
    "class"=>\Illuminate\Support\Str::slug($page->title)
])


@section("content")
    <div class="imus-page single">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h3>{{$page->title}}</h3>
                </div>
                <div class="card-body">
                    <p>
                        {!! $page->body !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection()
