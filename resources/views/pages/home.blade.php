<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/
?>

@extends("layouts.master",['title'=>getTitle("Trang chủ"),'class'=>"imus-page imus-home"])
@section("meta")
    <meta name="site_url" content="{{route("home")}}">
@endsection
@section("css")
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection
@section("content")

    <div class="col-sm-12 padd-right-15 hinhanh-sukien">
        <!-- 1 -->
        @include("components.home.slider")
        {{--@include("components.home.banner-1")--}}
        <div class="row">
            <div class="col-md-6">
                @include("components.home.post-1",["cat"=>"quy-hoc-bong-co-dien"])
            </div>
            <div class="col-md-6">
                @include("components.home.post-1",["cat"=>"quy-phat-trien-tai-nang"])
            </div>
        </div>

        <div class="search-box">
            @include("components.home.search")
        </div>
        <div class="news">
            @include("components.home.new-item",["cat"=>"tin-tuc-su-kien"])
        </div>

        <div class="best-of-member">
            <div class="title">
                <a href="{{route("imus.post.category","cuu-sinh-vien-tieu-bieu")}}">cựu sinh viên tiêu biểu</a>
            </div>
            <div class="member-slide-items">
                @foreach(Post::getPostByCat("tin-tuc-su-kien") as $member)
                    @include("components.home.member", ["member"=>$member])
                @endforeach
            </div>
        </div>

        <div class="partner-area ">
            <div class="partners">
                @foreach(getPartners() as $partner)
                    <div>
                        <div style="display: flex; justify-content: center">
                            <img src="{{asset("storage/".$partner->image)}}" alt="">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        @include("components.posts.album",['slideToShow'=>5])
        {{--        @include("components.home.post-in-cat",["cat"=>"cong-van-thong-bao"])--}}
        {{--        @include("components.home.post-in-cat",["cat"=>"tin-tuc-su-kien"])--}}
        {{--        @include("components.home.post-in-cat",["cat"=>"cuu-sinh-vien-voi-nha-truong"])--}}
        {{--        @include("components.home.post-in-cat",["cat"=>"nhung-lan-tho-ca-theo-cung-nam-thang"])--}}
    </div>
@endsection
@section("javascript")
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $(".member-slide-items").slick({
                slidesToShow: 4,
                slidesToScroll: 4,
                autoplay: true,
                autoplaySpeed: 2000,
                arrows: false,
                dots: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                        }
                    }
                ]
            });

            $(".partners").slick({
                slidesToShow: 6,
                slidesToScroll: 2,
                autoplay: true,
                autoplaySpeed: 1000,
                arrows: false,
                dots: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            })
        });

    </script>
@endsection
