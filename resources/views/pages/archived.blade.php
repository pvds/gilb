<?php
/**
 * @author kogoro
 * @created_at 9/5/19
 **/
?>
@extends("layouts.master",[
    "title"=>getTitle($cat->name),
    "class"=>\Illuminate\Support\Str::slug($cat->name)
])
@section("content")
    <div class="col-sm-9 col-xs-12 padd-right-15 mar-top-15">
        <div class="ms-webpart-zone ms-fullWidth">
            <div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                    <div>
                        <div id="">
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    function Redirecthispage() {
                                        var pathname = window.location.pathname;
                                        window.location.href = pathname;
                                    }
                                });
                            </script>


                            <div class="col-xs-12 none-padding p-list-page">
                                <h1>{{$cat->name}}</h1>
                                @if(count($posts->all())!=0)
                                    @foreach($posts as $post)
                                        <ul class="col-xs-12 none-padding p-list-content">
                                            <li>
                                                <a href="{{route("imus.post.show", $post->slug)}}">
                                                    <img src="{{asset("storage/".$post->image)}}"
                                                         alt="">
                                                </a>
                                                <a href="{{route("imus.post.show", $post->slug)}}"
                                                   title="{{$post->title}}"
                                                   class="p-name">
                                                    {{$post->title}}
                                                </a>
                                                <p class="p-des">{!! $post->excerpt !!}</p>
                                            </li>
                                        </ul>
                                    @endforeach

                                    <div class="col-xs-12 none-padding phantrang border-top-dotted">
                                        <div class="col-xs-12 none-padding">
                                            {{$posts->links()}}
                                        </div>
                                    </div>
                                @else
                                    Không có bài viết trong danh mục này.
                                    <a href="{{route("home")}}">Trang chủ </a>
                                @endif
                            </div>

                            <div class="col-xs-12 none-padding mar-top-15"></div>

                        </div>
                        <div class="ms-clear"></div>
                    </div>
                </div>
                <div class="ms-PartSpacingVertical"></div>
            </div>

        </div>
    </div>
@endsection
