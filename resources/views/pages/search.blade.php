@extends("layouts.full-width",[
    "title" => 'Tìm kiếm'
])

@section("content")
    <div class="imus-page single">
        <div>
            <div class="card">
                <div class="card-header">
                    <h3>Tìm kiếm</h3>
                </div>
                <div class="card-body" style="display: grid">
                    <div class="search-box">
                        <form id="student_search">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-8 form-group">
                                    <input type="text" class="form-control" id="student_name"
                                           name="full_name"
                                           value="{{$full_name}}"
                                           placeholder="Tên Cựu Sinh Viên" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 form-group">
                                    <select class="form-control" name="year">
                                        <option selected value="">Niên khóa</option>
                                        @foreach (students_filter('year') as $item)
                                            <option value="{{$item->year}}">{{$item->year}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2 form-group">
                                    <select class="form-control" name="educational">
                                        <option selected value="">Hệ đào tạo</option>
                                        @foreach (students_filter('educational') as $item)
                                            <option value="{{$item->educational}}">{{$item->educational}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2 form-group">
                                    <select class="form-control" name="course">
                                        <option selected value="">Khóa</option>
                                        @foreach (students_filter('course') as $item)
                                            <option value="{{$item->course}}">{{$item->course}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2 form-group">
                                    <select class="form-control" name="class">
                                        <option selected value="">Lớp</option>
                                        @foreach (students_filter('class') as $item)
                                            <option value="{{$item->class}}">{{$item->class}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <select class="form-control" name="association">
                                        <option selected value="">Hội Cơ điện</option>
                                        @foreach (students_filter('association') as $item)
                                            <option value="{{$item->association}}">{{$item->association}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <select class="form-control" name="work">
                                        <option selected value="">Đơn vị công tác</option>
                                        @foreach (students_filter('work') as $item)
                                            <option value="{{$item->work}}">{{$item->work}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2 form-group">
                                    <select class="form-control" name="address">
                                        <option selected value="">Nơi ở</option>
                                        @foreach (students_filter('address') as $item)
                                            <option value="{{$item->address}}">{{$item->address}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive table-responsive-lg" id="table">
                        @include('components.search.student_table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()

@section('javascript')
    <script>
        $(document).ready(function () {
            $('input').keyup(function () {
                get_students();
            });

            $('select').on('change', function () {
                get_students();
            })
        });

        function get_students() {
            let data = $('#student_search').serializeArray();
            console.log(data);
            let url = "{{route('search-ajax')}}";
            let success = function(result) {
                $('#table').empty().html(result);
            };
            $.post(url, data, success);
        }
    </script>
@stop
