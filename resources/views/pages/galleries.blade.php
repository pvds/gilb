@extends("layouts.master",[
    "title"=>getTitle("Thư viện hình ảnh"),
    "class"=>\Illuminate\Support\Str::slug("Thư viện hình ảnh"),
    "noPageNav"=>true,
    "noSidebar"=>true
])
@section("css")
    <style>
        .images {
            margin: 15px 0;
        }

        .images .img-item .picture {
            position: relative;
            height: 170px;
            overflow: hidden;
            object-fit: contain;
            display: flex;
            border: solid 4px #e9e9e9;
            padding: 1px;
            transition: all 2s;
        }

        .images .img-item:hover {
            cursor: pointer;
        }

        .images .picture:hover img {
            transform: scale(1.2);
            transition: all 2s;
        }

        .images .img-item {
            display: block;
            position: relative;
            padding: 5px;
        }

        .images .img-item .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            background: rgba(0, 0, 0, 0.5);
            height: 100%;
            display: none;
            align-items: center;
            justify-content: center;
        }

        .images .img-item .overlay a {
            margin: 5px;
            color: white;
        }
    </style>
@endsection
@section("content")
    <div class="imus-page single">
        <div class="container">
            <div class="tab">
                <ul class="nav nav-tabs">
                    <li class="nav-item active">
                        <a href="#" class="nav-link" data-toggle="tab" data-tab="all">Tất cả</a>
                    </li>
                    @foreach($groups as $item=>$value)
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab"
                               href="#" data-tab="{{\Illuminate\Support\Str::slug($item)}}">{{$item}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="tab-content">
                <div class="images">
                    <div class="row" style="margin: 0">
                        @foreach($groups as $item => $images)
                            @foreach($images as $img)
                                <a href="{{asset("storage/".$img['picture'])}}"
                                   class="col-sm-6 col-md-3 img-item {{\Illuminate\Support\Str::slug($item)}}">
                                    <div class="picture">
                                        <img style="width: 100%" src="{{asset("storage/".$img['picture'])}}" alt="">
                                        {{--<div class="overlay d-flex align-item-center justify-content-center">
                                            <a href=""><i class="fa fa-eye"></i></a>
                                            <a target="_blank" href="{{$img['post_link']}}"><i
                                                        class="fa fa-link"></i></a>
                                        </div>--}}
                                    </div>
                                </a>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()
@section("script")
    <link rel="stylesheet" href="{{asset("assets/magific-popup/magnific-popup.css")}}">
    <script src="{{asset("assets/magific-popup/jquery.magnific-popup.min.js")}}"></script>
    <script>
        $(document).ready(function () {

            $(".images").magnificPopup({
                type: "image",
                delegate: "a",
                gallery: {enabled: true}
            });

            $(".nav-link").click(function (e) {
                let target = e.target.getAttribute("data-tab");
                if (target == "all") {
                    $(".images .img-item").show();
                } else {
                    $('.images .img-item').each(function (index, item) {
                        if ($(item).hasClass(target))
                            $(item).fadeIn();
                        else
                            $(item).fadeOut();
                    });
                }
            });


        })
    </script>
@endsection
