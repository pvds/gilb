@extends("layouts.master",[
    "title"=>getTitle($post->title),
    "class"=>\Illuminate\Support\Str::slug($post->title)
])
@section("meta")
    <meta name="post_title" content="{{$post->title}}">
    <meta name="post_url" content="{{route("imus.post.show", $post->slug)}}">
@endsection
@section("css")
{{--    @include("components.posts.style")--}}
@endsection
@section("content")
    <div class="imus-page single col-sm-9 col-xs-12 padd-right-15">
        <div class="ms-webpart-zone ms-fullWidth">
            <div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                    <div style="">
                        <div id="">
                            <div style="display: none;">
                                <div id="ninja-slider">
                                    <div class="slider-inner">
                                        <ul id="ulImg"></ul>
                                        <div id="fsBtn" class="fs-icon" title="Đóng"></div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: none;" class="hiddent-container">
                                <div id="dialog-form">
                                </div>
                                <div id="dialog-form-2">
                                </div>
                                <div id="dialog-form-3">
                                </div>
                                <div id="dialog-confirm">
                                </div>
                                <div id="dialog-message">
                                </div>
                                <div id="message-container">
                                </div>
                                <div id="dialog-loading"></div>
                            </div>
                            <div class="col-xs-12 none-padding tinchitiet">
                                <div class="col-xs-12 none-padding">
                                    <div class="col-xs-12 none-padding">
                                        <p class="tieu-de">{{$post->title}}</p>
                                    </div>
                                    {{--<div class="col-xs-12 none-padding border-bottom">
                                        <div class="col-xs-3 none-padding ">
                                            <p>{{$post->created_at}}</p>
                                        </div>

                                        <div class="col-xs-8 none-padding btn-left text-right">
                                            <ul class="p-view">
                                                <li><p>xem cỡ chữ</p></li>
                                                <li><p id="zoomnho" class="text b-tb">T</p></li>
                                                <li><p id="zoomto" class="text b-tt">T</p></li>
                                                <li style="padding: 0 2px;"><a href="#0" onclick="do_something()"><img
                                                                src="{{asset("images/intrang.svg")}}"> In trang</a></li>
                                                <li><p>lượt xem: 4995 </p></li>
                                            </ul>
                                        </div>

                                    </div>--}}
                                </div>

                                <!-- tomtat -->
                                <div class="col-xs-12 none-padding " id="contenttin">
                                    <div>{!! $post->body !!}</div>

                                    <p class="tacgia"><span>Tác giả: </span><span
                                                style="font-weight: 600">{{author($post->author_id)->name}}</span>
                                    </p>
                                    <div class="col-xs-12 none-padding item-col-hoidap mar-top-15">
                                        <!--File đính kèm-->

                                        <!--File đính kèm-->

                                    </div>


                                    <!-- end tomtat -->
                                </div>
                            </div>
                            <!-- tin bai lien quan -->
                        @include("components.posts.relate-post")
                        <!-- end tin bai lien quan -->
                            <div class="col-xs-12 none-padding text-right">
                                <ul class="share-post">
                                    <li><a href="#0"><img src="{{asset("images/facebook.png")}}"></a></li>
                                    <li><a href="#0"><img src="{{asset("images/twitter.png")}}"></a></li>
                                    <li><a href="#0"><img src="{{asset("images/googleplus.png")}}"></a></li>
                                    <li><a href="#0"><img src="{{asset("images/print.png")}}"></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="ms-PartSpacingVertical"></div>
            </div>

            @include("components.posts.album")

        </div>
    </div>
@endsection()
@section("script")
{{--    @include("components.posts.script")--}}
@endsection
