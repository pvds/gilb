@extends("voyager::master")

@section('page_title', $title)

@section('page_header')
    <h1 class="page-title">
        <?php echo $title; ?>
    </h1>
@stop
@section('head')
    <link rel="stylesheet" href="{{asset("public/assets/admin/admin-custom.css")}}">

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
@stop()
@section("content")
    <div id="imusPage__<?php echo $id; ?>" class="imus-admin-page page-<?php echo $class; ?>">
        @yield("imus-content")
    </div>
@endsection
