<div class="panel">
    <div class="panel-body panel-galleries">
        <div class="card">
            <div class="card-header">
                <h4>Add Image</h4>
            </div>
            <div class="card-body" style="padding:15px 0">

                <form action="{{route("voyager.galleries.store")}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="col-md-12">
                        <label for="name">Name</label>
                        <input type="text"
                               required
                               placeholder="Name"
                               name="name" class="form-control">
                    </div>

                    <div class="col-md-12">
                        <label for="parent_id">Group</label>
                        <input type="hidden" value="" name="group">
                        <div style="position: relative">
                            <input type="text" id="fakeGroup"
                                   placeholder="-- None --"
                                   class="form-control">
                            <ul id="fakeSelection" class="card">
                                <li data-value="">-- None --</li>
                                @foreach (getGalleryGroup() as $item)
                                    <li data-value="{{$item->group}}">{{$item->group}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label for="description">Description</label>
                        <textarea name="description" id="" cols="30" rows="5" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12">
                        <label for="slug">Image</label>
                        <input type="file" style="padding: 0"
                               required
                               name="picture" class="">
                    </div>

                    <input type="datetime" style="display: none" class="form-control datepicker" name="created_at"
                           value="">
                    <div class="col-md-12" style="text-align: right">
                        <button class="btn btn-primary">Thêm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
