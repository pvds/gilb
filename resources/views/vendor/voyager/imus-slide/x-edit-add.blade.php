<div class="panel">
    <div class="panel-body" style="padding: 15px 35px">
        <h4>Add Slide</h4>
        <form action="{{route("voyager.imus-slide.store")}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="margin-bottom-15">
                <label for="name">Name</label>
                <input type="text"
                       required
                       name="name" id="" class="form-control">
            </div>

            <div class="margin-bottom-15">
                <label for="src">Image</label>
                <input style="padding: 0; border: none"
                       accept="image/*"
                       type="file" name="src" id="" class="form-control">
            </div>

            <div class="margin-bottom-15">
                <label for="group">Slider</label>
                <select name="group" id="" class="form-control">
                    <?php foreach (getSliders(["id", "name"]) as $slider): ?>
                    <option value="{{$slider['id']}}">{{$slider['name']}}</option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="margin-bottom-15">
                <label for="order">Order</label>
                <input type="number"
                       required
                       name="order" id="" class="form-control">
            </div>

            <div class="margin-bottom-15 position-relative">
                <label for="order">Post</label>
                <input type="hidden"
                       name="post_id" id="" class="form-control">

                {{-- input fake --}}
                <input type="text"
                       class="form-control iFaker"
                       data-fake="posts" data-ftype="selection" data-col="title" data-target="post_id"
                       placeholder="Try with some keyword.." id="">
            </div>

            <input type="datetime" style="display: none" class="form-control datepicker" name="created_at" value="">
            <div class="col-md-12" style="text-align: right">
                <button class="btn btn-primary">SAVE</button>
            </div>
        </form>
    </div>
</div>
