<div class="panel">
    <div class="panel-body" style="padding: 15px 35px">
        <h4>Add Category</h4>
        <form action="{{route("voyager.categories.store")}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}


            <div class="col-md-3">
                <label for="name">Name</label>
                <input type="text"
                       required
                       placeholder="Name"
                       name="name" id="catName" class="form-control">
            </div>

            <div class="col-md-3">
                <label for="slug">Slug</label>
                <input type="text"
                       required
                       placeholder="Slug"
                       name="slug" id="catSlug" class="form-control">
            </div>

            <div class="col-md-3">
                <label for="parent_id">Parent</label>
                <select name="parent_id" id="" class="form-control">
                    <option value="">-- None --</option>
                    <?php foreach (getCategories(["id", "name"]) as $cat): ?>
                    <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-md-3">
                <label for="order">Order</label>
                <input type="number"
                       required
                       placeholder="Order"
                       name="order" id="" class="form-control">
            </div>

            <input type="datetime" style="display: none" class="form-control datepicker" name="created_at" value="">
            <div class="col-md-12" style="text-align: right">
                <button class="btn btn-primary">SAVE</button>
            </div>
        </form>
    </div>
</div>

