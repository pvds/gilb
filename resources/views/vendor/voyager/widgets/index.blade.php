<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/
?>

@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.__('voyager::generic.settings'))

@section('css')
    <style>
        .code {
            border-radius: 30px;
            padding: 5px 10px;
            font-size: 11px;
            border: 0;
            top: -2px;
        }

        .add-new-widget .form-control {
            width: 20%;
            display: inline-flex;
        }

        .card-item {
            margin-bottom: 10px;
        }

        .card-head {
            position: relative;
        }

        .card-item .ibtn {
            z-index: 999;
            position: absolute;
            top: 6px;
            padding: 2px 4px;
            background: #8f8f8f;
            color: white;
            font-size: 10px;
            border-radius: 5px;
        }

        .card-item .ibtn.delete {
            right: 4px;
        }

        .card-item .ibtn.code {
            right: 55px;
        }

        .card-item .card-body {
            display: none;
        }

        h4.card-title:hover {
            cursor: pointer;
        }

        h4.card-title {
            padding: 6px 10px;
            margin-top: 0;
            background: #ddd;
            margin-bottom: 0;
        }

        .card .card-body {
            padding: 10px;
        }

        .card .card-body .btn-area {
            width: 100%;
            text-align: right;
        }

        .shortcodes .card-body:hover {
            cursor: copy;
        }

        .shortcodes .card-body {
            padding: 10px;
            z-index: 2;
            background: #ff00009e;
            position: relative;
            color: white;
        }

        .shortcodes .card-head {
            height: 130px;
        }

        .shortcodes .card-head img {
            width: 100%;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-paw"></i> Widgets
    </h1>
@stop
@section('head')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
@stop()
@section('head')

@stop()

@section('content')
    <div class="container-fluid">
        @include('voyager::alerts')
        @can('add', new \App\Models\Admin\Widgets())
            <div class="panel">
                <div class="panel-body">
                    <form action="{{ route('imus-widget-add') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="col-md-3">
                            <label for="display_name">Display name</label>
                            <input class="form-control" type="text" name="name" id="" placeholder="Ex: AdsInFooter"
                                   required>
                        </div>
                        <div class="col-md-3">
                            <label for="name">Description</label>
                            <input class="form-control" type="text" name="description" id="" placeholder="Description"
                                   required>
                        </div>
{{--                        <div class="col-md-3">--}}
                        {{--                            <label for="name">Status</label>--}}
                        {{--                            <input class="form-check" type="checkbox" name="status" id="">--}}
                        {{--                        </div>--}}
                        <div class="col-md-3">
                            <label for="type">{{ __('voyager::generic.type') }}</label>
                            <select class="form-control" name="type" id="" required>
                                <option value="shortcode">Shortcodde</option>
                                <option value="text">Text</option>
                                <option value="html">HTML tag</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary"
                                    style="position: absolute;bottom: -65px;right: 15px;">
                                <i class="voyager-plus"></i> Add new widget
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        @endcan

        <div class="row">
            <div class="shortcodes col-sm-6">
                <h4>Available widgets</h4>
                <div class="row">
                    @foreach($shortcodes as $shortcode=> $value)
                        <article class="card col-md-6">
                            <div class="card-head">
                                <img src="{{$value['img']}}"
                                     alt="">
                            </div>
                            <div class="card-body btn-copy" data-clipboard-text="[{{$value['code']}}]">
                                <span>{{$value['name']}}</span>
                            </div>
                        </article>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    @foreach($widgets as $w)
                        <div id="item__{{$w->id}}" class="card-item">
                            <div class="card-head">
                                <h4 class="card-title">{{$w->name}}</h4><span
                                        class="btn-copy ibtn code"
                                        data-clipboard-text="<?php echo "@widget('" . toWidgetName($w->name) . "')"; ?>">Copy code</span>
                                <span class="voyager-trash ibtn delete"
                                      data-id="{{ $w->id }}"> Delete</span>
                            </div>
                            <div class="card-body widgets-body">
                                <form action="{{route('imus-widget-update')}}" method="post">
                                    <span class="desc">{{$w->description}}</span>
                                    {{csrf_field()}}
                                    <input type="hidden" name="_id" value="{{$w->id}}">
                                    @if($w->type=="html")
                                        <textarea data-type="{{$w->type}}" class="form-control richtext__box"
                                                  name="content" id=""
                                                  cols="30"
                                                  rows="10">@if(!empty($w->content)){!! $w->content !!}@endif</textarea>
                                    @elseif($w->type=="text")
                                        <textarea data-type="{{$w->type}}" class="form-control richtext__box"
                                                  name="content" id=""
                                                  cols="30"
                                                  rows="10">@if(!empty($w->content)){!! $w->content !!}@endif</textarea>
                                    @elseif($w->type=="shortcode")
                                        <input data-type="{{$w->type}}" class="form-control" type="text" name="content"
                                               value="@if(!empty($w->content)){!! $w->content !!}@endif">
                                    @endif
                                    <div class="btn-area">
                                        <input type="submit" class="btn btn-primary" value="Save">
                                    </div>
                                </form>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script src='https://cloud.tinymce.com/5/tinymce.min.js?apiKey=your_API_key'></script>
@stop

@section('javascript')

    <script>
        $(document).ready(function () {

            $('.card-head').click(function () {
                $(this).parent().find('.card-body.widgets-body').slideToggle();
            });

            // tinymce.init({selector: '#__demo'});

            $('.ibtn.delete').click(function () {
                let removed = confirm("Are you want to delete item?");

                if (removed) {
                    let id = $(this).data('id');
                    $.ajax({
                        url: "{{route('imus-widget-remove')}}",
                        type: "post",
                        data: {
                            _token: "{{csrf_token()}}",
                            id: id
                        }, success: function (res) {
                            //location.reload();
                            $(`#item__${id}`).fadeOut(300);
                            $(`#item__${id}`).remove();
                        }
                    });
                }
            });

            new ClipboardJS('.btn-copy');
            $('.btn-copy').click(function () {
                $('.btn-copy.ibtn').text("Copy code");
                if ($(this).hasClass('ibtn')) {
                    $(this).text('Copied');
                }
            });
        });
    </script>
@stop
