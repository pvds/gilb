@include('laravote::stubs.style')
<div class="laravote-box">
    <form method="POST" action="{{ route('poll.vote', $id) }}">
        @csrf
        <div class="laravote-heading">
            <span class="laravote-title">
                {{ $question }}
            </span>
        </div>
        <div class="laravote-body">
            <div class="laravote-desc">
                <span>{{ $description }}</span>
            </div>
            <ul class="list-group">
                @foreach($options as $id => $name)
                    <li class="list-group-item">
                        <div class="radio">
                            <label>
                                <input value="{{ $id }}" type="checkbox" name="options[]">
                                {{ $name }}
                            </label>
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="laravote-footer">
                <input type="submit" class="btn-vote" value="Vote"/>
            </div>
        </div>
    </form>
</div>
