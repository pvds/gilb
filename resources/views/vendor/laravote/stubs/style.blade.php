<style>
    .laravote-heading {
        background: #4267b2;
        text-align: center;
        color: white;
        text-transform: uppercase;
        font-weight: 600;
        padding: 5px 0;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }

    .laravote-body {
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px;
        padding: 10px;
        border: 1px solid #4267b2;
    }

    .laravote-footer {
        text-align: center;
    }

    input.btn-vote {
        padding: 5px 40px;
        border-radius: 5px;
        background: #4267b2;
        color: white;
        font-size: 16px;
        font-weight: 600;
    }

    input.btn-vote:hover {
        background: #4267b2;
    }

    .laravote-desc {
        padding: 15px 5px;
        font-size: 15px;
        font-weight: 700;
    }
</style>
