<?php
/**
 * @author: Nong Van Du
 * desc:
 */
?>
<div id="side-banner">
    <div class="side-banner-left sticky">
        <img src="{{asset("storage/".option('site.banner_left'))}}" alt="">
    </div>
    <div class="side-banner-right sticky">
        <img src="{{asset("storage/".option('site.banner_right'))}}" alt="">
    </div>
</div>

