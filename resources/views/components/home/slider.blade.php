<?php
/**
 * @author kogoro
 * @created_at 8/31/19
 **/
?>
<div class="ms-webpart-zone ms-fullWidth">
    <div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div>
                <div id="">
                    <div class="col-xs-12 none-padding  box-shadow-bg news-xl">
                        <div class="col-sm-12 none-padding">
                            <ul class=" head-list ">
                                <li>
                                    {{--<a href="#"><img src="{{asset("images/mui_ten_do.svg")}}">TIN TỨC - SỰ KIỆN</a>--}}
                                </li>

                            </ul>
                        </div>
                        {{--                        @dd(slider("Home Slider"))--}}
                        <div class="col-xs-12 none-padding">
                            <div id="slide1" class="carousel slide slider-new"
                                 data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    @foreach(slider("Home Slider") as $slide)
                                        <li data-target="#slide1" data-slide-to="{{$loop->index}}"
                                            class="{{ $loop->index==0?"active":""}}"></li>
                                    @endforeach
                                </ol>

                                <div class="carousel-inner">
                                    @foreach(slider("Home Slider") as $slide)
                                        <?php $post = Post::getPostById($slide->post_id); ?>
                                        {{--@dd($post)--}}
                                        <div class="item {{$loop->index==0?"active":""}}">
                                            <a href="{{route("imus.post.show", $post->slug)}}">
                                                <span class="new-img">
                                                    <img style="height:unset; vertical-align:top;" src="{{asset("storage/".$post->image)}}">
                                                </span>
                                                <span class="new-text">
                                                    <h4 class="tieude">{{$post->title}}</h4>
                                                    <label class="tomtat">{{$post->excerpt}}</label>
                                                </span>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>


                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#slide1"
                                   data-slide="prev">

                                </a>
                                <a class="right carousel-control" href="#"
                                   data-slide="next">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- marquee -->

                    <script type="text/javascript">
                        $(document).ready(function () {
                            var width = $(document).width();
                            var slider = $('#slider-news').bxSlider({
                                pager: false,
                                speed: 1000,
                                pause: 10000,
                                mode: 'fade',
                                slideWidth: width,
                                infiniteLoop: true,
                                captions: true,
                                onSlideNext: function () {
                                    slider.startAuto();
                                },
                                onSlidePrev: function () {
                                    slider.startAuto();
                                },
                                auto: true,
                                autoHover: true
                            });

                            $("#slider-home-new").bxSlider({
                                pager: false,
                                speed: 1000,
                                slideWidth: width,
                                infiniteLoop: true,
                                captions: true,
                                nextSelector: 'null',
                                prevSelector: 'null',
                            });

                            $("#item-img-1").attr("class", "col-md-3 clear-padding item-img active");
                            var sliderVideo = $('#slider-videos').bxSlider({
                                slideWidth: width,
                                infiniteLoop: true,
                                pagerCustom: '#bx-pager',
                                video: true,
                                captions: true,
                                // onSlideNext: function () {
                                // var current = sliderVideo.getCurrentSlide() + 1;
                                // var count = sliderImg.getSlideCount();
                                // clearActiveItemImg(count);
                                // $("#item-img-" + current).attr("class", "col-md-3 clear-padding item-img active");
                                // },
                                nextSelector: 'null',
                                prevSelector: 'null',
                                autoStart: false
                            });
                            var current = 1;

                            var sliderImg = $('#slider-img').bxSlider({
                                minSlides: 4,
                                maxSlides: 4,
                                speed: 1000,
                                slideWidth: width,
                                infiniteLoop: true,
                                margin: 5,
                                nextSelector: '#slider-next',
                                prevSelector: 'null',
                                nextText: "<i class='fa fa-caret-right pull-right'></i>",
                                onSliderLoad: function () {
                                    $("#ImgCurrent").text(current + "/");
                                },
                                onSlideNext: function () {
                                    current++;
                                    var CountImg = parseInt(document.getElementById("ImgCount").innerText);
                                    if (current == (CountImg + 1)) {
                                        current = 1;
                                    }
                                    $("#ImgCurrent").text(current + "/");
                                }
                            });
                            actionClickVideoItem();

                            function clearActiveItemImg(count) {
                                for (var i = 1; i <= count; i++) {
                                    $("#item-img-" + i).attr("class", "col-md-3 clear-padding item-img");
                                }
                            }

                            function actionClickVideoItem() {
                                $(".item-img").click(function (event) {
                                    event.preventDefault();
                                    $("#main-video iframe").css("display", "none");
                                    $("#main-video video").css("display", "none");
                                    var count = $("#bx-pager ul").find("li").length;
                                    clearActiveItemImg(count);
                                    var id = $(this).attr("id");
                                    $("#" + id).attr("class", "col-md-3 clear-padding item-img active");
                                    var url = $("#" + id + " a").attr("data-url");
                                    var text = $("#" + id + " a").attr("data-title");
                                    var data = $("#" + id + " a").attr("datacheck");

                                    if (data == "1") {
                                        $("#main-video iframe").attr("src", url);
                                        $("#main-video iframe").load();
                                        $("#main-video iframe").css("display", "block");
                                    } else {
                                        $("#main-video video").attr("src", url);
                                        $("#main-video video").load();
                                        $("#main-video video").css("display", "block");
                                    }
                                    $("#caption p").text(text);

                                })
                            }

                        });
                    </script>
                </div>
                <div class="ms-clear"></div>
            </div>
        </div>
        <div class="ms-PartSpacingVertical"></div>
    </div>

</div>
