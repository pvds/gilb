<?php
/**
 * @author kogoro
 * @created_at 8/31/19
 **/
?>
<?php $posts = Post::getPostByCat($cat, 7); ?>
@if(count($posts)>0)
    <div class="ms-webpart-zone ms-fullWidth">
        <div id=""
             class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
            <div
                    class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                <div id=""
                     width="100%" class="ms-WPBody " allowdelete="false" allowexport="false"
                     style="">
                    <div>
                        <div class="col-xs-12 none-padding mar-top-15">
                            <div class="col-sm-12 none-padding">
                                <ul class="col-sm-12 none-padding ul-tab">
                                    <li class="col-sm-12 none-padding active"><a
                                                href="{{route("imus.post.category", $cat)}}">{{$posts[0]->name}}</a></li>
                                </ul>
                                <div class="col-xs-12 none-padding tab-content  none-border-radius nab-tab-tcyk bg-new">
                                    <div id="tab240" class="tab-pane fade active in">
                                        <div class="col-xs-12 none-padding">

                                            <div class="col-xs-12 none-padding hoatdong hd-right">
                                                <ul class="col-xs-12 none-padding thongbao">
                                                    @foreach($posts as $post)
                                                        <li class="col-xs-12 none-padding">
                                                            <h4 class="col-xs-12 none-padding tieude-bold"
                                                                style="width: 94%">
                                                                <a href="{{route("imus.post.show", $post->slug)}}"
                                                                   title="{{$post->title}}">{{$post->title}}</a>
                                                            </h4>
                                                        </li>
                                                    @endforeach
                                                    {{--<li class="no-ic">
                                                        <a href="{{route("imus.post.category", $cat)}}"
                                                           class="news-main__more hover-u"><i>&gt;&gt;
                                                                xem tất cả</i></a>
                                                    </li>--}}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="ms-clear"></div>
                </div>
            </div>
            <div class="ms-PartSpacingVertical"></div>
        </div>
    </div>
@endif
