<?php
/**
 * @author kogoro
 * @created_at 9/4/19
 **/
?>
<div class="ms-webpart-zone ms-fullWidth">
    <div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div>
                <div id="">
                    <script type="text/javascript">
                        $(document).ready(function () {
                            var fullDate = new Date();

                            var twoDigitMonth = ((fullDate.getMonth() + 1) < 10) ? ('0' + (fullDate.getMonth() + 1)) : (fullDate.getMonth() + 1);
                            var twoDigitDate = ((fullDate.getDate() + 0) < 10) ? ('0' + fullDate.getDate()) : fullDate.getDate();
                            var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
                            $('#Currentdate').text(DayOfWeek() + ', ngày ' + currentDate);

                            $("#btnSearchHome").click(function () {
                                var kwd = $("#searchHome").val();
                                window.location.href = '/pages/tim-kiem.aspx?kwd=' + kwd;
                                return false;
                            });

                            $("#searchHome").keyup(function (event) {
                                if (event.keyCode == 13) {
                                    var kwd = $("#searchHome").val();
                                    window.location.href = '/pages/tim-kiem.aspx?kwd=' + kwd;
                                }
                            });
                        });

                        function DayOfWeek() {
                            var DayOfWeek = new Date().getDay();
                            switch (DayOfWeek) {
                                case 0:
                                    return "Chủ nhật";
                                case 1:
                                    return "Thứ hai";
                                case 2:
                                    return "Thứ ba";
                                case 3:
                                    return "Thứ tư";
                                case 4:
                                    return "Thứ năm";
                                case 5:
                                    return "Thứ Sáu";
                                case 6:
                                    return "Thứ bảy";
                            }
                        }

                        function Redirecthispage() {
                            var protocol = window.location.protocol;
                            var host = window.location.host;
                            var Url = protocol + '//' + host;
                            window.location.href = Url;
                        }
                    </script>
                    @if(isset($catId))
                        <?php $cat = getCatsTree($catId); //dd($cat);?>
                        <div class="col-xs-12">
                            <div class="col-xs-12 none-padding breadcrumb mar-top-10">
                                <ul class="col-xs-12 none-padding">

                                    <li class="col-xs-12 none-padding active">
                                        <a class="selected active"
                                           href="javascript:Redirecthispage()">{{setting('site.title')}}</a>
                                    </li>

                                    <?php
                                    do{
                                    $active = $cat['id'] == $catId ? "active" : ""; ?>

                                    <li class="col-xs-12 none-padding <?php echo $active; ?>">
                                        <a class="selected <?php echo $active; ?>"
                                           href="{{route("imus.post.category", $cat['slug'])}}"><?php echo $cat['name']; ?></a>
                                    </li>

                                    <?php $cat = $cat['parent_id'];
                                    } while ($cat['parent_id'] != null);
                                    ?>

                                </ul>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
