<div class="card item">
    <div class="card-body">
        <img style="width: 100%" src="{{asset("storage/".$member->image)}}" alt="">
    </div>
    <div class="card-footer">
        <a href="{{route("imus.post.show", $member->slug)}}"><strong>{{\Illuminate\Support\Str::limit($member->title, 55)}}</strong></a>
    </div>
</div>
