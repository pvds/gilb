<?php
/**
 * @author: Nong Van Du
 * desc:
 */
?>

<div class="ms-webpart-zone ms-fullWidth">
    <div id=""
         class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
        <div
                class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
            <div>
                <div>
                    <div class="col-xs-12 none-padding mar-top-15">
                        <div class="col-sm-12 none-padding">
                            <ul class="col-sm-12 none-padding ul-tab">
                                <li class="col-sm-12 none-padding active"><a
                                            href="javascript:;">Tìm Kiếm Cựu Sinh Viên</a></li>
                            </ul>
                            <div class="col-xs-12 none-padding tab-content  none-border-radius nab-tab-tcyk bg-new"
                                 style="min-height: 100px;">
                                <div id="tab240" class="tab-pane fade active in" style="padding-top: 20px;">
                                    <div class="col-xs-12 none-padding">
                                        <form action="{{route('search')}}" method="POST">
                                            {{csrf_field()}}
                                            <div class="col-md-5 form-group">
                                                <input type="text" class="form-control" id="student_name"
                                                       name="full_name"
                                                       placeholder="Tên Cựu Sinh Viên" required>
                                            </div>
                                            <div style="text-align: left" class="col-md-3 form-group">
                                                <button type="submit" class="btn btn-primary"
                                                        style="background: var(--primary-color)">Tìm Kiếm
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="ms-clear"></div>
            </div>
        </div>
        <div class="ms-PartSpacingVertical"></div>
    </div>
</div>
