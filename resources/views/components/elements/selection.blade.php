<?php
/**
 * @author kogoro
 * @created_at 8/31/19
 **/
?>
<select name="" id="{{$param['table']}}_{{$param['type']}}" class="form-control">
    @foreach($data as $item)
        <option value="{{$item->id}}"><?php echo $item->{$col}; ?></option>
    @endforeach
</select>

