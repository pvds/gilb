<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/
?>
{{-- BEGIN COOKIE CONSENT --}}
<link rel="stylesheet" type="text/css"
      href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css"/>
<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
    window.addEventListener("load", function () {
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#2b80b5",
                    "text": "#ffffff"
                },
                "button": {
                    "background": "#f5d948"
                }
            },
            "theme": "classic",
            "content": {
                "message": "{{__("lang.cookie - message")}}",
                "dismiss": "{{__("lang.cookie - dismiss")}}",
                "link": "{{__("lang.cookie - link")}}",
                "href": "http://vietnam.imus.vn/en/terms-and-conditions.html"
            }
        })
    });
</script>
{{-- END COOKIE CONSENT! --}}
