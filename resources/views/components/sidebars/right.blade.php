<?php
/**
 * @author kogoro
 * @created_at 8/31/19
 **/
?>
<div class="col-sm-3 col-xs-12 none-padding padd-right-15">
    <div class="ms-webpart-zone ms-fullWidth">
        {{-- THONG BAO --}}
        <div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
            <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                <div>
                    <div>
                        <div class="col-xs-12 none-padding sidebar-box">
                            <div class="col-xs-12 none-padding tin-thongbao">
                                <a href="https://hcma.vn/vanban/Pages/van-ban-quan-ly.aspx?CateID=255">THÔNG BÁO</a>
                            </div>
                            <div class="col-xs-12 none-padding frame-bg frame-thongbao  ">
                                <ul class="col-xs-12 none-padding none-margin thongbao hoatdong ul-thanhvien-si">
                                    @foreach(Post::getPostByCat("cong-van-thong-bao") as $post)
                                        <li class="col-xs-12 none-padding">
                                            <a href="{{route("imus.post.show", $post->slug)}}"
                                               class="col-xs-12 none-padding"
                                               title="{{$post->title}}">{{$post->title}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <script type="text/javascript">
                            function rediectnam() {
                                debugger;
                                var urlpagesl = $("#Selectnam option:selected").attr("urlpagesl");

                                var namselect = $('#Selectnam').val();
                                window.location = urlpagesl + "?CateID=" + namselect;
                            }
                        </script>
                    </div>
                    <div class="ms-clear"></div>
                </div>
            </div>
            <div class="ms-PartSpacingVertical"></div>
        </div>
        {{-- VOTE --}}
        <div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
            <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                <div>
                    <div>

                        <div class="col-xs-12 none-padding mar-top-15">
                            [poll id="1"]
                        </div>
                    </div>
                    <div class="ms-clear"></div>
                </div>
            </div>
            <div class="ms-PartSpacingVertical"></div>
        </div>

        {{-- BANNER --}}
        <div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
            <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                <div>
                    <div>
                        <div class="col-xs-12 none-padding mar-top-15">
                            <div class="col-xs-12 none-padding banner_chaomung banner-codong">
                                <a href="https://hcma.vn/tintuc/pages/dien-dan-chinh-tri-tu-tuong.aspx">
                                    <img src="{{asset("storage/".option('site.banner_248x74'))}}">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="ms-clear"></div>
                </div>
            </div>
            <div class="ms-PartSpacingVertical"></div>
        </div>

        {{-- FEATURE POST --}}
        <div id="" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
            <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                <div>
                    <div>
                        <div class="col-xs-12 none-padding mar-top-15 sidebar-box">
                            <div class="col-xs-12 none-padding tin-thongbao">
                                <a href="#">BÀI VIẾT NỔI
                                    BẬT</a>
                            </div>
                            <div class="col-xs-12 none-padding bg-grey-hd">
                                <marquee class="sidebar-list2 sidebar-list--srcoll block"
                                         behavior="scroll" align="center" direction="up"
                                         scrollamount="2" scrolldelay="5"
                                         onmouseover="this.stop()" onmouseout="this.start()"
                                         ontouchstart="this.stop()" ontouchend="this.start()"
                                         width="100%">
                                    <ul class="col-xs-12 none-padding thongbao hoatdong ul-thanhvien">

                                        @foreach(Post::featurePosts() as $post)
                                            <li class="col-xs-12 none-padding">
                                                <a href="{{route("imus.post.show", $post->slug)}}"
                                                   class="col-xs-12 none-padding"
                                                   title="{{$post->title}}">{{$post->title}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div style="width: 100%; height: 240px"></div>
                                </marquee>
                            </div>
                        </div>


                        <script type="text/javascript">
                            function rediectnam() {
                                debugger;
                                var urlpagesl = $("#Selectnam option:selected").attr("urlpagesl");

                                var namselect = $('#Selectnam').val();
                                window.location = urlpagesl + "?CateID=" + namselect;
                            }
                        </script>
                    </div>
                    <div class="ms-clear"></div>
                </div>
            </div>
            <div class="ms-PartSpacingVertical"></div>
        </div>

    </div>

    {{-- PARTNER --}}
    <div class="ms-webpart-zone ms-fullWidth">
        <div id="" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
            <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                <div width="100%" class="ms-WPBody " allowdelete="false" allowexport="false"
                     style="">
                    <div id="">
                        <div class="col-xs-12 none-padding mar-top-15">
                            <ul class="col-xs-12 none-padding ul-congthongtin ul-danhmuc ">
                                @foreach(getPartners() as $partner)
                                    <li class="col-xs-12 none-padding">
                                        <a href="#" class="col-xs-12 none-padding"
                                           target="_blank">
                                        <span class="col-xs-3 none-padding" style="padding: 3px;">
                                            <img src="{{asset("storage/".$partner->image)}}">
                                        </span>
                                            <span class="col-xs-9 none-padding">{{$partner->name}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="ms-clear"></div>
                </div>
            </div>
            <div class="ms-PartSpacingVertical"></div>
        </div>

        {{-- THONG KE --}}
        <div id=""
             class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
            <div
                    class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                <div webpartid="976236f8-9697-4656-9a84-83b580382301" haspers="false"
                     id="WebPartctl00_ctl36_g_976236f8_9697_4656_9a84_83b580382301"
                     width="100%" class="ms-WPBody " allowdelete="false" allowexport="false"
                     style="">
                    <div id="ctl00_ctl36_g_976236f8_9697_4656_9a84_83b580382301">


                        <!-- thong ke truy cap -->
                        <div
                                class="col-xs-12 none-padding mar-top-15 thongketruycap sidebar-box">

                            <div class="col-xs-12 none-padding ">
                                <ul class="col-xs-12 none-padding ul-thongke">
                                    <li>Thống kê truy cập</li>
                                    <li>
                                        Lượt truy cập:<span>499740</span>
                                    </li>
                                    <li>
                                        Khách online: <span> 57</span>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <!-- end thong ke truy cap-->


                    </div>
                    <div class="ms-clear"></div>
                </div>
            </div>
            <div class="ms-PartSpacingVertical"></div>
        </div>
    </div>
</div>
