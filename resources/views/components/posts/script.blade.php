<?php
/**
 * @author kogoro
 * @created_at 9/4/19
 **/
?>
{{--<script type="text/javascript"--}}
{{--        src="/Publishing_Resources/BinhLuan/jquery.validate.js"></script>--}}
{{--<script type="text/javascript" src="/Publishing/js/jquery-ui-1.9.2.custom.js"></script>--}}
{{--<script type="text/javascript"--}}
{{--        src="/Admin_Resources/plugins/select2/select2.min.js"></script>--}}
{{--<script type="text/javascript"--}}
{{--        src="/Admin_Resources/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>--}}
{{--<script type="text/javascript"--}}
{{--        src="/Admin_Resources/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>--}}
{{--<script type="text/javascript" src="/Publishing/plugins/notify/js/notify.js"></script>--}}
{{--<script type="text/javascript"--}}
{{--        src="/Publishing_Resources/js/plugins/colorbox-master/jquery.colorbox.js"></script>--}}
<script type="text/javascript">
    $(document).ready(function () {
        $(".img-class img").each(function () {
            $(this).attr("href", $(this).attr("src"));
        });
        var fontdefault = 12;
        var foncurrent;
        $("#zoomto").click(function () {
            if (foncurrent < 0 || foncurrent == 'undefined' || foncurrent == '' || foncurrent == null) {
                foncurrent = fontdefault;
            }
            foncurrent = foncurrent + 2;
            console.log($("#contenttin"));
            $("#contenttin p").css("font-size", foncurrent);
            $("#contenttin p span").css("font-size", foncurrent);
        });
        $("#zoomnho").click(function () {
            if (foncurrent > fontdefault) {
                foncurrent = foncurrent - 2;
                $("#contenttin p").css("font-size", foncurrent);
                $("#contenttin p span").css("font-size", foncurrent);
            }
        });
    });
    setTimeout(function () {
        $(".img-class img").colorbox({
            rel: 'group3',
            transition: "none",
            width: "75%",
            height: "75%",
            next: "Sau",
            previous: "Trước",
            close: "Đóng",
            current: "{current} / {total}"
        });
    }, 100);

    //show popup
    function lightbox() {

        var ninjaSldr = document.getElementById("ninja-slider");
        ninjaSldr.parentNode.style.display = "block";

        nslider.init();

        var fsBtn = document.getElementById("fsBtn");
        fsBtn.click();
    }

    function fsIconClick(isFullscreen, ninjaSldr) { //fsIconClick is the default event handler of the fullscreen button
        if (isFullscreen) {
            ninjaSldr.parentNode.style.display = "none";
        }
    }

    $(function () {
        $(".listContent li").each(function () {

            $(this).find("span").click(function () {
                //alert($(this).attr("dataUrl"));
                $("#pdf-viewer #plugin").attr("src", $(this).attr("dataUrl"));

            })

        })
    })
</script>
<script type="text/javascript">

    $(document).ready(function () {

        var idArr = [];
        idArr = document.getElementsByTagName("td");
        for (var i = 0; i < idArr.length; i++) {
            var tagtd = idArr[i];
            tagtd.rowSpan = 1;
        }

        var urlFormComemnt = "";
        var urlListComemnt = "";
        var urlSendMail = "";
        if ("False" == "False") {
            urlFormComemnt = "/UserControls/Publishing/News/BinhLuan/pFormBL.aspx?UrlListProcess=/content/tintuc/Lists/News&ItemID=29508&UrlListProcessBL=/content/tintuc/Lists/CommentsNews&IsTA=False";
            urlListComemnt = "/UserControls/Publishing/News/BinhLuan/pDanhSachComment.aspx?UrlListProcessBL=/content/tintuc/Lists/CommentsNews&ItemID=29508&IsTA=False";
            urlSendMail = "/UserControls/Publishing/News/BinhLuan/pFormSendEmail.aspx?UrlListProcess=/content/tintuc/Lists/News&ItemID=29508";

        } else {

            urlFormComemnt = "/UserControls/Publishing/News/BinhLuan/pFormBLEng.aspx?UrlListProcess=/content/tintuc/Lists/News&ItemID=29508&UrlListProcessBL=/content/tintuc/Lists/CommentsNews&IsTA=False";
            urlListComemnt = "/UserControls/Publishing/News/BinhLuan/pDanhSachComment.aspx?UrlListProcessBL=/content/tintuc/Lists/CommentsNews&ItemID=29508&IsTA=False";
            urlSendMail = "/UserControls/Publishing/News/BinhLuan/pFormSendEmailEng.aspx?UrlListProcess=/content/tintuc/Lists/News&ItemID=29508";
        }
        //execute('form - comment');
        loadAjaxContent(urlFormComemnt, "#form-comment");
        //LoadAjaxPage(urlListComemnt, "#Danh-sach-comment");
        hide("form-comment");

        $("#SendMail").click(function () {
            //console.log(!$.trim($("#dialog-form-2").html()));

            $.post(encodeURI(urlSendMail), function (data) {
                $("#dialog-form-2").html(data);
            });
            $("#dialog-form-2").dialog(
                {title: "Email", width: 600, height: 420}
            ).dialog("open");
            return false;
        });

        // phân trang bình luận
        var rows_per_page = 5;
        var countbl = 0;
        var total_rows = Math.ceil(countbl / 5);
        //var total_rows = 50;
        initPageNumbers(total_rows, rows_per_page);

        //Set the default page number
        var page_num = 1;

        //If there's a hash fragment specifying a page number
        if (window.location.hash !== '') {
            //Get the hash fragment as an integer
            var hash_num = parseInt(window.location.hash.substring(1));

            //If the hash fragment integer is valid
            if (hash_num > 0) {
                //Overwrite the default page number with the user supplied number
                page_num = hash_num;
            }
        }
        getPage(page_num);

    });

    function initPageNumbers(total_rows, rows_per_page) {
        $('#page-numbers').append("<li class='startpage'><a href='#'> <i class='fa fa-chevron-left' aria-hidden='true'></i> </a></li>");

        for (var x = 1; x <= total_rows; x++) {
            $('#page-numbers').append('<li class="paggingli' + x + '" style="display:none"><a href="#" class="paggingmenu' + x + '" onclick="getPage(' + x + ');" >' + x + '</a></li>');
        }
        $('#page-numbers').append("<li class='endpage'><a href='#' onclick='getendpage(" + 1 + ")'> <i class='fa fa-chevron-right' aria-hidden='true'></i> </a></li>");
    }

    function getPage(page_num) {
        var countbl = 0;
        var total_rows = Math.ceil(countbl / 5);
        //var total_rows = 50;
        if (page_num == 1) {
            $(".startpage").css("display", "none");
        } else {
            $(".startpage").css("display", "block");
        }
        if ((total_rows - page_num) == 0) {
            $(".endpage").css("display", "none");
        } else {
            $(".endpage").css("display", "block");
        }
        for (var i = 1; i <= total_rows; i++) {
            if (i == page_num) {
                $(".pagging" + i).css("display", "block");
                $(".paggingmenu" + i).addClass("a_activer");
            } else {
                $(".pagging" + i).css("display", "none");
                $(".paggingmenu" + i).removeClass("a_activer");
            }
        }
        for (var a = 1; a <= total_rows; a++) {
            $(".paggingli" + a).css("display", "none");
        }
        if (page_num > 3) {
            if ((total_rows - page_num) >= 2) {
                for (var x = (page_num - 2); x <= (page_num + 2); x++) {
                    $(".paggingli" + x).css("display", "block");
                }
            } else {
                for (var x = total_rows; x > (total_rows - 5); x--) {
                    $(".paggingli" + x).css("display", "block");
                }
            }
            $(".endpage a").attr("onclick", "getendpage(" + total_rows + "," + (page_num + 1) + ")");
            $(".startpage a").attr("onclick", "getstartpage(" + total_rows + "," + (page_num - 1) + ")");
        } else {
            if (total_rows <= 5) {
                for (var x = 1; x <= total_rows; x++) {
                    $(".paggingli" + x).css("display", "block");
                }

            } else {
                for (var x = 1; x <= 5; x++) {
                    $(".paggingli" + x).css("display", "block");
                }

            }
            $(".endpage a").attr("onclick", "getendpage(" + total_rows + "," + (page_num + 1) + ")");
            $(".startpage a").attr("onclick", "getstartpage(" + total_rows + "," + (page_num - 1) + ")");
        }
    }

    function getendpage(total_rows, page_num) {
        getPage(page_num);
    }

    function getstartpage(total_rows, page_num) {
        getPage(page_num);
    }

    function do_something() {
        if (!$.trim($("#dialog-form").html())) {
            $("#dialog-form").dialog({
                title: "Print",
                width: $(window).width(),
                height: $(window).height()
            }).load(encodeURI("/UserControls/Publishing/News/BinhLuan/pFormPrint.aspx?UrlListProcess=/content/tintuc/Lists/News&ItemID=29508&IsTA=False")).dialog("open");
        } else {
            $("#dialog-form").parent().css("display", "block");
        }
    }


    var flag = 1;

    function LoadAjaxPage(urlPage, container) {
        $.post(
            urlPage,
            function (data) {
                $(container).html(data)
            });
    }

    function execute(element) {
        if (flag == 1) {
            var bien = $("#form-comment");
            show(element);
            flag = 0;
        } else if (flag == 0) {
            $("#form-comment").css("display", "block");
        }

    }

    function show(element) {
        var tag = document.getElementById(element);

        if (tag == null) return;

        tag.style.display = 'block';
    }

    function hide(element) {
        var tag = document.getElementById(element);
        if (tag == null) return;
        tag.style.display = 'none';
    }

    function loadAjaxContent(urlContent, container) {
        $.ajax({
            url: encodeURI(urlContent),
            cache: false,
            type: "POST",
            success: function (data) {
                $(container).html(data);
            }
        });
    }

    // Gửi Email
    var flagEmail = 1;

    function executeEmail(element) { //current element
        if (flagEmail == 1) {
            showEmail(element);
            flagEmail = 0;
        } else if (flagEmail == 0) {
            hideEmail(element);
            flagEmail = 1;
        }
    }

    function showEmail(element) {
        var tag = document.getElementById(element);
        if (tag == null) return;
        tag.style.display = 'block';
        initAjaxLoad(urlFormEmail, "#form-comment");
    }

    function hideEmail(element) {
        var tag = document.getElementById(element);
        if (tag == null) return;
        tag.style.display = 'none';
    }

    //End Gửi Email
    function showformComment() {
        initAjaxLoad(urlListComemnt, "#Danh-sach-comment");
    }

    //Tao hoi thoai thong bao
    function createMessage(title, message) {
        $("#dialog-message").attr("title", title);
        $("#dialog-message").html("<p>" + message + "</p>");
        $("#dialog-message").dialog({
            resizable: false,
            height: 160,
            modal: true,
            buttons: {
                "Đóng lại": function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    function CreateMessageSuscess(title, message) {
        $("#dialog-message").attr("title", title);
        $("#dialog-message").html("<p>" + message + "</p>");
        $("#dialog-message").dialog({
            resizable: false,
            height: 160,
            modal: true,
            buttons: {
                "Đóng lại": function () {
                    $(this).dialog("close");
                    location.reload();
                }
            }
        });
    }

    var checkHienthi = 1;

    function hienthithem() {
        if (checkHienthi == 1) {
            $(".display_none").css("display", "block");
            $(".btn_hienThi").html("Rút gọn");
            checkHienthi = 0;
        } else {
            $(".display_none").css("display", "none");
            $(".btn_hienThi").html("Hiển thị thêm");
            checkHienthi = 1;
        }
        return false;
    }

</script>
