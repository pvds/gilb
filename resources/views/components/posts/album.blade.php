<?php
/**
 * @author kogoro
 * @created_at 9/4/19
 **/
?>

<div class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
    <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
        <div>
            <div>
                <div class="col-xs-12 none-padding mar-top-15 album-anh-video">
                    <div class="col-sm-12 none-padding">
                        <ul class="col-sm-12 none-padding ul-tab">
                            <li class="col-sm-12 none-padding " href="#albumanh">
                                <a href="{{route("galleries")}}">ALBUM ẢNH</a>
                            </li>
                        </ul>

                        <!--/myCarousel Album Anh-->
                        <div class="col-xs-12 none-border-radius bg-new bg-grey" style="padding: 16px">
                            <div id="albumanh" class="tab-pane fade in active">
                                <div class="col-xs-12 none-padding mar-top-15">
                                    <div class="col-xs-12 none-padding">
                                        <div id="album" class="slick-slide-wrapper">
                                            <!-- Carousel items -->
                                            @foreach(galleries() as $g)
                                                <div class="slick-slide-img-wrapper">
                                                    <img style="object-fit: contain; min-height: 155px"
                                                         src="{{asset("storage/".$g->picture)}}">
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/myCarousel Album Video-->
                        {{--<div id="video" class="tab-pane fade in ">
                            <div class="col-xs-12 none-padding mar-top-15">
                                <div id="videoclip" class="carousel slide slide-video"
                                     data-ride="carousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">

                                        <div class="item active left">

                                            <div class="col-xs-6 col-sm-4 col-md-3 bg-menu">
                                                <a href="/tintuc/video/Pages/default.aspx?ItemID=18">
                                                    <img src="https://filehvtt.hcma.vn:443/pic/Hinhanh-Video/tải xuống_20171210101819PM.jpg">
                                                </a>
                                            </div>

                                            <div class="col-xs-6 col-sm-4 col-md-3 bg-menu">
                                                <a href="/tintuc/video/Pages/default.aspx?ItemID=16">
                                                    <img src="https://filehvtt.hcma.vn:443/pic/Hinhanh-Video/Capture_20171210100829PM.PNG">
                                                </a>
                                            </div>

                                            <div class="col-xs-6 col-sm-4 col-md-3 bg-menu">
                                                <a href="/tintuc/video/Pages/default.aspx?ItemID=14">
                                                    <img src="https://filehvtt.hcma.vn:443/pic/GDDT/news/khaigiangk34_20171207021516PM.jpg">
                                                </a>
                                            </div>

                                            <div class="col-xs-6 col-sm-4 col-md-3 bg-menu">
                                                <a href="/tintuc/video/Pages/default.aspx?ItemID=15">
                                                    <img src="https://filehvtt.hcma.vn:443/pic/GDDT/news/Viện KT_20171211104401AM.jpg">
                                                </a>
                                            </div>

                                            <!--/row-->
                                        </div>

                                        <div class="item next left">

                                            <div class="col-xs-6 col-sm-4 col-md-3 bg-menu">
                                                <a href="/tintuc/video/Pages/default.aspx?ItemID=17">
                                                    <img src="https://filehvtt.hcma.vn:443/pic/Hinhanh-Video/hoc-vien-chinh-tri-quoc-gia-ho-chi-minh-can-tam-nhin-phat-trien-moi_20171210101351PM.jpg">
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                    <!--/carousel-inner-->
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#videoclip"
                                       data-slide="prev">
                                        <img src="/Publishing/images/pre.svg">
                                    </a>
                                    <a class="right carousel-control" href="#videoclip"
                                       data-slide="next">
                                        <img src="/Publishing/images/next.svg">
                                    </a>
                                </div>
                            </div>
                        </div>--}}
                        <!--/myCarousel Album Video-->
                        </div>

                    </div>
                </div>
            </div>
            <div class="ms-clear"></div>
        </div>
    </div>
</div>
@push("css-code")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
@endpush
@section("script")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <script>
        $("#album").slick({
            infinite: true,
            slidesToShow: "{{isset($slideToShow)?$slideToShow:3}}",
            slidesToScroll: 1,
            autoplay: true,
            arrows: false,
            autoplaySpeed: 1000,
            centerPadding: "20px",
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        centerPadding: '15px',
                        slidesToShow: "{{isset($slideToShow)?FLOOR($slideToShow/2):3}}"
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        centerPadding: '10px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>
@endsection
