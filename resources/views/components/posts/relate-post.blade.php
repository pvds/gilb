<?php
/**
 * @author kogoro
 * @created_at 9/4/19
 **/
?>
<?php

$posts = getPostBy((object)[
    "key" => "category_id",
    "value" => $catId
], ["id", "title", "slug"]);
?>
<div class="col-xs-12 none-padding mar-top-20">
    <div class="col-xs-12 none-padding">
        <ul class=" head-list border-ul ul-img-tinlq">
            <li>
                <a href="#">TIN BÀI LIÊN QUAN</a>
            </li>

        </ul>
    </div>
    <div class="col-xs-12 none-padding hoatdong ">
        <ul class="col-xs-12 none-padding thongbao  ul-none-border ul-tinlq">

            @foreach($posts as $post)
                <li class="col-xs-12 none-padding ">
                    <h4 class="col-md-12 col-sm-12 none-padding " style="width:93%;">
                        <a style="line-height: 30px;" href="{{route("imus.post.show", $post->slug)}}">{{$post->title}}</a>
                    </h4>
                </li>
            @endforeach
        </ul>

    </div>
</div>
