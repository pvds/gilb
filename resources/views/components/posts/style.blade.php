<?php
/**
 * @author kogoro
 * @created_at 9/4/19
 **/
?>
{{--<link type="text/css"--}}
{{--      href="/Admin_Resources/plugins/bootstrap-modal/css/bootstrap-modal.css"--}}
{{--      rel="stylesheet">--}}
{{--<link type="text/css" href="/Publishing/plugins/notify/css/notify-flat.css"--}}
{{--      rel="stylesheet">--}}

{{--<link href="/Publishing_Resources/js/plugins/colorbox-master/example4/colorbox.css"--}}
{{--      rel="stylesheet">--}}
<style>
    table {
        width: 675px !important;
    }

    div#pdf-viewer {
        width: 99%;
        height: 777px;
        border: solid 1px #bcbcbc;
        z-index: 10;
        margin: 0 auto;
    }

    #dialog-form {
        border: 1px solid #999;
        background-color: white;
        margin: 0 auto !important;
    }

    .div_form_binhLuans > form > div > textarea {
        width: 100%;
        /*font-size: 11px;*/
        height: 100px;
        padding: 5px;
    }

    .clear-padd-left {
        padding-left: 0px !important;
    }

    .div_form_binhLuans .text_note_bl {
        margin-left: 10px;
        /*font-size: 11px;*/
        font-style: italic;
        font-weight: 500;
    }

    .div_form_binhLuans input {
        width: 95%;
        /*font-size: 11px;*/
        padding: 3px 5px;
    }

    .marg-top-10 {
        margin-top: 10px !important;
    }

    .div_form_binhLuans .btn_huy {
        color: rgb(160, 160, 160);
        background-color: transparent;
        border-color: rgb(160, 160, 160);
        border-width: 1px;
        border-style: solid;
        padding: 3px 20px;
        /*font-size: 11px;*/
    }

    .div_form_binhLuans .btn_gui {
        color: #1EAF5F;
        background-color: transparent;
        border-color: #1EAF5F;
        border-width: 1px;
        border-style: solid;
        padding: 3px 15px;
        margin-left: 15px;
        /*font-size: 11px;*/
    }

    .clear-padd-right {
        padding-right: 0px !important;
    }

    .div_form_binhLuans .btn_gui > i {
        margin-right: 10px;
    }
</style>
<style>
    table td {
        Rows: 1;
    }

    .div_form_binhLuans {
        padding-top: 20px;
    }
</style>
