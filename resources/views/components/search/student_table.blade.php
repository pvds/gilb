<?php
/**
 * @author: Nong Van Du
 * desc:
 */
?>
<style>
    .dataTables_length {
        display: none;
    }

    #students_box_filter {
        display: none;
    }
</style>

@if (count($students))
    <table class="table table-striped table-bordered" id="students_box">
        <thead>
        <tr>
{{--            <th>STT</th>--}}
            <th>Họ và Tên</th>
            <th>SĐT</th>
            <th>Email</th>
            <th>Niên khóa</th>
            <th>Hệ đào tạo</th>
            <th>Khóa</th>
            <th>Lớp</th>
            <th>Hội Cơ điện</th>
            <th>BLL</th>
            <th>Chức vụ</th>
            <th>Đơn vị công tác</th>
            <th>Nơi ở</th>
        </tr>
        </thead>
        <tbody>
        @foreach($students as $key=>$student)
            <tr>
{{--                <td>{{$key+1}}</td>--}}
                <td>{{$student->full_name}}</td>
                <td>{{$student->phone}}</td>
                <td>{{$student->email}}</td>
                <td>{{$student->year}}</td>
                <td>{{$student->educational}}</td>
                <td>{{$student->course}}</td>
                <td>{{$student->class}}</td>
                <td>{{$student->association}}</td>
                <td>{{$student->bll}}</td>
                <td>{{$student->position}}</td>
                <td>{{$student->work}}</td>
                <td>{{$student->address}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p style="color: red; font-size: 20px; margin-top: 45px;text-align: center">Không có kết quả!</p>
@endif

<script>
    $(document).ready(function () {
        $('#students_box').DataTable({
            "language": {
                "info": "Hiển thị trang _PAGE_/_PAGES_",
                "paginate": {
                    "first": "Đầu",
                    "last": "Cuối",
                    "next": "Sau",
                    "previous": "Trước"
                },
            }
        });
    });
</script>
