<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

// Route::redirect('/forum', '/forum');

Route::get('auth/other/', 'ThirdPartyController@receive');
Route::post('auth/other/login', 'ThirdPartyController@login')->name('third-party-login');

Route::get('auth/other/response', function () {
    $redirect = session('redirect');
    $avatar = asset('public/storage/' . Auth::user()->avatar);
    $username = Auth::user()->name;
    $email = Auth::user()->email;

    $url = $redirect . "?code=200&username=" . $username . "&email=" . $email . "&avatar_url=" . $avatar;

    return Redirect::to($url);
})->name('third-party-response');

Route::get('/', function () {
    return view('frontend.index')->withShortcodes();
})->name("home");


// Route::group(['prefix' => 'admin'], function () {
//     Voyager::routes();

//     // HTTP HANDLER REQUEST
//     Route::any("/http-handler", "Admin\HttpController@handle")->name("admin.http.handler");

//     // WIDGETS
//     Route::get("/widgets", "Admin\WidgetController@index")->name('imus-widget');
//     Route::post('/widgets/add', "Admin\WidgetController@add")->name('imus-widget-add');
//     Route::post('/widgets/update', "Admin\WidgetController@update")->name('imus-widget-update');
//     Route::post('/widgets/remove', "Admin\WidgetController@remove")->name('imus-widget-remove');

//     // OPTIONS
//     Route::get("/options", "Admin\OptionController@index")->name('imus.option');
//     Route::put("/options/update", "Admin\OptionController@update")->name('imus.option.update');
//     Route::post("/options/delete", "Admin\OptionController@remove")->name('imus.option.delete');
//     Route::post("/options/store", "Admin\OptionController@store")->name('imus.option.store');
// });

// // page
// Route::get("/pages/{page}", "PageController@show")->name("imus.pages");

// // post
// Route::get('/tin-tuc/danh-muc/{slug}', "PostController@category")->name("imus.post.category");
// Route::get('/tin-tuc/{slug}', "PostController@show")/*->middleware("post")*/ ->name("imus.post.show");

// // Galleries
Route::get("/contact", 'PageController@contact')->name("contact");
Route::get("/about", 'PageController@about')->name("about");
Route::get("/blog", 'PageController@blog')->name("blog");
Route::get("/single-blog", 'PageController@singleBlog')->name("single-blog");

// Route::any('/search', 'SearchController@search')->name('search');

// Route::post('/search-ajax', 'SearchController@search_ajax')->name('search-ajax');
