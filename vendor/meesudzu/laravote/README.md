# Laravote
A Laravel package to manage your polls  
(A clone of inani/larapoll)
- require tcg/voyager
- Add vote description
- Change stars_at, ends_at timestamp to date
- Add isExpried

## Installation:
First, install the package through Composer. 

```bash
composer require meesudzu/laravote 
```

You can skip the next two steps

Then include the service provider inside `config/app.php`.

```php
'providers' => [
    ...
    Meesudzu\Laravote\LarapollServiceProvider::class,
    ...
];


'aliases' => [
        ...
        'PollWriter' => Meesudzu\Laravote\PollWriterFacade::class,
        ...
];
```


Publish migrations, and migrate

```bash
php artisan vendor:publish
php artisan migrate
```

___
## Use
- ID poll, ex: 1  
``{{ PollWriter::draw(1) }}``
- Route: url/admin/polls
