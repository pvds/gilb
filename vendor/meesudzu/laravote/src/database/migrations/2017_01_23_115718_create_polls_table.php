<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravote_polls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->text('description')->nullable();
            $table->integer('maxCheck')->default(1);
            $table->boolean('canVisitorsVote')->default(0);
            $table->timestamp('isClosed')->nullable();
            $table->date('starts_at')->nullable();
            $table->date('ends_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('laravote_polls');
    }
}
