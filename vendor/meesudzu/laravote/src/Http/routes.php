<?php

$prefix = config('laravote.prefix');
Route::group(['namespace' => '\Meesudzu\Laravote\Http\Controllers', 'prefix' => $prefix, 'middleware' => 'web'], function(){

    $middleware = config('laravote.admin_auth');

    $guard = config('laravote.admin_guard');
    Route::middleware(["$middleware:$guard"])->group(function () {
        Route::get('/polls-index', ['uses' => 'PollManagerController@home', 'as' => 'poll.home']);
        Route::get('/polls', ['uses' => 'PollManagerController@index', 'as' => 'poll.index']);
        Route::get('/polls/create', ['uses' => 'PollManagerController@create', 'as' => 'poll.create']);
        Route::get('/polls/{poll}', ['uses' => 'PollManagerController@edit', 'as' => 'poll.edit']);
        Route::patch('/polls/{poll}', ['uses' => 'PollManagerController@update', 'as' => 'poll.update']);
        Route::delete('/polls/{poll}', ['uses' => 'PollManagerController@remove', 'as' => 'poll.remove']);
        Route::patch('/polls/{poll}/lock', ['uses' => 'PollManagerController@lock', 'as' => 'poll.lock']);
        Route::patch('/polls/{poll}/unlock', ['uses' => 'PollManagerController@unlock', 'as' => 'poll.unlock']);
        Route::post('/polls', ['uses' => 'PollManagerController@store', 'as' => 'poll.store']);
        Route::get('/polls/{poll}/add', ['uses' => 'OptionManagerController@push', 'as' => 'poll.options.push']);
        Route::post('/polls/{poll}/add', ['uses' => 'OptionManagerController@add', 'as' => 'poll.options.add']);
        Route::get('/polls/{poll}/remove', ['uses' => 'OptionManagerController@delete', 'as' => 'poll.options.remove']);
        Route::delete('/polls/{poll}/remove', ['uses' => 'OptionManagerController@remove', 'as' => 'poll.options.remove']);
    });

    Route::post('/vote/polls/{poll}', 'VoteManagerController@vote')->name('poll.vote');
});
