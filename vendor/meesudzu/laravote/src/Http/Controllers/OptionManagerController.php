<?php

namespace Meesudzu\Laravote\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Meesudzu\Laravote\Helpers\PollHandler;
use Meesudzu\Laravote\Http\Request\AddOptionsRequest;
use Meesudzu\Laravote\Poll;

class OptionManagerController extends Controller
{
    /**
     * Add new options to the poll
     *
     * @param Poll $poll
     * @param AddOptionsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Poll $poll, AddOptionsRequest $request)
    {
        $poll->attach($request->get('options'));

        return redirect(route('poll.index'))
            ->with('success', 'New poll options have been added successfully');
    }

    /**
     * Remove the Selected Option
     *
     * @param Poll $poll
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Poll $poll, Request $request)
    {
        try{
            $poll->detach($request->get('options'));
            return redirect(route('poll.index'))
                ->with('success', 'Poll options have been removed successfully');
        }catch (Exception $e){
            return back()->withErrors(PollHandler::getMessage($e));
        }
    }

    /**
     * Page to add new options
     *
     * @param Poll $poll
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function push(Poll $poll)
    {
        return view('laravote::dashboard.options.push', compact('poll'));
    }

    /**
     * Page to delete Options
     *
     * @param Poll $poll
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Poll $poll)
    {
        return view('laravote::dashboard.options.remove', compact('poll'));
    }
}
