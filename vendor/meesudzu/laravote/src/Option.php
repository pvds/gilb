<?php

namespace Meesudzu\Laravote;

use Illuminate\Database\Eloquent\Model;
use Meesudzu\Laravote\Traits\Votable;

class Option extends Model
{
    use Votable;

    protected $fillable = ['name'];

    protected $table = 'laravote_options';
    /**
     * An option belongs to one poll
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }

    /**
     * Check if the option is Closed
     *
     * @return bool
     */
    public function isPollClosed()
    {
        return $this->poll->isLocked();
    }
}
