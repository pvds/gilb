<?php
namespace Meesudzu\Laravote\Traits;


use Meesudzu\Laravote\Poll;

trait PollWriterResults
{
    /**
     * Draw the results of voting
     *
     * @param Poll $poll
     */
    public function drawResult(Poll $poll)
    {
        $total = $poll->votes->count();
        $results = $poll->results()->grab();
        $options = collect($results)->map(function ($result) use ($total){
                return (object) [
                    'votes' => $result['votes'],
                    'percent' => $total === 0 ? 0 : ($result['votes'] / $total) * 100,
                    'name' => $result['option']->name
                ];
        });
        $question = $poll->question;
        $description = $poll->description;

        echo view(config('laravote.results') ? config('laravote.results') : 'laravote::stubs.results', compact('options', 'question', 'description'));
    }
}
