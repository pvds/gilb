<?php
namespace Meesudzu\Laravote\Traits;

use Illuminate\Support\Facades\Session;
use Meesudzu\Laravote\Poll;

trait PollWriterVoting
{
    /**
     * Drawing the poll for checkbox case
     *
     * @param Poll $poll
     */
    public function drawCheckbox(Poll $poll)
    {
        $options = $poll->options->pluck('name', 'id');

        echo view(config('laravote.checkbox') ? config('laravote.checkbox') :  'laravote::stubs.checkbox', [
            'id' => $poll->id,
            'question' => $poll->question,
            'description' => $poll->description,
            'options' => $options
        ]);
    }

    /**
     * Drawing the poll for the radio case
     *
     * @param Poll $poll
     */
    public function drawRadio(Poll $poll)
    {
        $options = $poll->options->pluck('name', 'id');

        echo view(config('laravote.radio') ? config('laravote.radio') :'laravote::stubs.radio', [
            'id' => $poll->id,
            'question' => $poll->question,
            'description' => $poll->description,
            'options' => $options
        ]);
    }
}
