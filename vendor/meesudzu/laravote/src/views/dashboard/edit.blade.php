@extends('voyager::master')
@section('page_title', 'Polls- Edit')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
@endsection
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-group"></i> Poll Edit
        </h1>
    </div>
@stop
@section('content')
    <div class="page-content browse container-fluid">
        <div class="well col-md-12">
        @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form method="POST" action=" {{ route('poll.update', $poll->id) }}">
            {{ csrf_field() }}
            @method('patch')
            <div class="form-group">
                <label>Question: </label>
                <textarea id="question" name="question" cols="30" rows="2" class="form-control" placeholder="Ex: Who is the best player in the world?">{{ old('question', $poll->question) }}</textarea>
            </div>
            <div class="form-group">
                <label for="question">Description:</label>
                <textarea id="question" name="description" cols="30" rows="2" class="form-control" placeholder="Write something description">{{ old('description', $poll->description) }}</textarea>
            </div>
            <div class="form-group">
                <label>Options</label>
                <ul class="options">
                    @foreach($poll->options as $option)
                    <li>
                        <input class="form-control add-input" value="{{ $option->name }}" disabled />
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="form-group">
                <label>Number of options to be selected</label>
                <select name="count_check" class="form-control">
                    @foreach(range(1, $poll->optionsNumber()) as $i)
                    <option {{ $i==$poll->maxCheck? 'selected':'' }}>{{ $i }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group clearfix">
                <label>Options</label>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="starts_at">Starts at:</label>
                        <input type="date" id="starts_at" name="starts_at" class="form-control" value="{{ old('starts_at', \Carbon\Carbon::parse($poll->starts_at)->format('Y-m-d')) }}" />
                    </div>

                    <div class="form-group col-md-6">
                        <label for="ends_at">Ends at:</label>
                        <input type="date" id="ends_at" name="ends_at" class="form-control" value="{{ old('ends_at', \Carbon\Carbon::parse($poll->ends_at)->format('Y-m-d')) }}" />
                    </div>
                </div>
            </div>

            <div class="radio">
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="canVisitorsVote" value="1" {{ old('canVisitorsVote', $poll->canVisitorsVote)  == 1 ? 'checked' : ''  }}> Allow to guests
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="close" {{ old('close', $poll->isLocked()) ? 'checked':'' }}> Close
                    </label>
                </div>
{{--                <div class="form-group">--}}
{{--                    <label>--}}
{{--                        <input type="checkbox" name="canVoterSeeResult" {{ old('canVoterSeeResult', $poll->showResultsEnabled()) ? 'checked':'' }}> Can visitor see result--}}
{{--                    </label>--}}
{{--                </div>--}}
            </div>

            <!-- Create Form Submit -->
            <div class="form-group">
                <input name="update" type="submit" value="Update" class="btn btn-primary create-btn" />
            </div>
        </form>
    </div>
</div>
@endsection
