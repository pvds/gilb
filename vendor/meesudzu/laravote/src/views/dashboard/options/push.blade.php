@extends('voyager::master')
@section('page_title', 'Polls- Add New Option')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
@endsection
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-group"></i> Poll Add New Option
        </h1>
    </div>
@stop
@section('content')
    <div class="page-content browse container-fluid">
        <div class="well col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    <span>Please fill empty inputs</span>
                </div>
            @endif
            <form method="POST" action=" {{ route('poll.options.add', $poll->id) }}">
                {{ csrf_field() }}
                <!-- Question Input -->
                <div class="form-group">
                    <label for="question">{{ $poll->question }}</label>
                </div>
                <div class="form-group">
                    <label>Options</label>
                    <ul class="options">
                        @foreach($poll->options as $option)
                            <li>
                                <input class="form-control add-input" value="{{ $option->name }}" disabled />
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="form-group">
                    <ul id="options">
                        <li>
                            <input type="text" name="options[0]" class="form-control add-input" placeholder="Insert your option" />
                            <a class="btn btn-danger" href="#" onclick='remove(this)'>
                                <i class="fa fa-minus-circle" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <ul>
                    <li class="button-add">
                        <div class="form-group">
                            <a class="btn btn-success" id="add">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            </a>
                        </div>
                    </li>
                </ul>
                <!-- Create Form Submit -->
                <div class="form-group">
                    <input name="Add" type="submit" value="Add" class="btn btn-primary create-btn" >
                </div>
            </form>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function remove(current){
            current.parentNode.remove()
        }
        document.getElementById("add").onclick = function() {
            var e = document.createElement('li');
            e.innerHTML = "<input type='text' name='options[]' class='form-control add-input' placeholder='Insert your option' /> <a class='btn btn-danger' href='#' onclick='remove(this)'><i class='fa fa-minus-circle' aria-hidden='true'></i></a>";
            document.getElementById("options").appendChild(e);
        }
    </script>
@endsection
