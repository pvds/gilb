<?php

namespace Meesudzu\Laravote\Helpers;

use Illuminate\Support\Facades\Auth;
use Meesudzu\Laravote\Guest;
use Meesudzu\Laravote\Poll;
use Meesudzu\Laravote\Traits\PollWriterResults;
use Meesudzu\Laravote\Traits\PollWriterVoting;

class PollWriter
{
    use PollWriterResults,
        PollWriterVoting;

    /**
     * Draw a Poll
     *
     * @param Poll $poll
     * @return string
     */
    public function draw($poll)
    {
        if(is_int($poll)){
            try {
                $poll = Poll::findOrFail($poll);
            } catch (\Exception $e) {
                return "This poll do not exist!";
            }
        }

//        if(!$poll instanceof Poll){
//            throw new \InvalidArgumentException("The argument must be an integer or an instance of Poll");
//        }

        if ($poll->isComingSoon()) {
            return 'To start soon';
        }

//        if (!$poll->showResultsEnabled()) {
//            return 'Thanks for voting';
//        }

        if (Auth::check()) {
             $voter = auth(config('laravote.admin_guard'))->user();
        } elseif ($poll->canGuestVote()) {
            $voter = new Guest(request());
        } else {
            return 'Not allow guest to vote';
        }

        if (is_null($voter) || $voter->hasVoted($poll->id) || $poll->isLocked() || $poll->isExpried()) {
            return $this->drawResult($poll);
        }

        if ($poll->isRadio()) {
            return $this->drawRadio($poll);
        }
        return $this->drawCheckbox($poll);
    }
}
