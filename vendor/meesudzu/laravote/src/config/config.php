<?php
return [
    'admin_auth' => env('LARAVOTE_ADMIN_AUTH_MIDDLEWARE', 'admin.user'),
    'admin_guard' => env('LARAVOTE_ADMIN_AUTH_GUARD', 'web'),
    'pagination' => env('LARAVOTE_PAGINATION', 15),
    'prefix' => env('LARAVOTE_PREFIX', 'admin'),
    'results' => '',
    'radio' => '',
    'checkbox' => '',
    'user_model' => App\User::class,
];
