<?php

namespace Meesudzu\Laravote;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'user_id', 'option_id'
    ];
    protected $table = 'laravote_votes';

    public function option()
    {
        return $this->belongsTo(Option::class);
    }
}
