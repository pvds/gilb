<?php

namespace App\Http\Middleware;

use Closure;
use TCG\Voyager\Models\Post;

class PostChecking
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request, $catId);
//        dd($request->getRequestUri());
        $requestUri = $request->getRequestUri();

        $slug = substr($requestUri, strrpos($requestUri, '/') + 1, strlen($requestUri));
        $post = Post::select("category_id")
            ->where("slug", $slug)
            ->first();
        if ($post) {
            $catId = $post->category_id;
            return $next($request, $catId);
        } else
            abort(404);
    }
}
