<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/

use Illuminate\Support\Facades\DB;

if (!function_exists('toWidgetName')) {
    /**
     * input: ClassName.php
     * @param $className
     * @return: className
     */
    function toWidgetName($className)
    {
        if (strpos($className, '\\') > 0) {
            $className = substr($className, strrpos($className, '\\') + 1, strlen($className));
        }
        return strtolower(substr($className, 0, 1)) . substr($className, 1, strlen($className));
    }
}

//
if (!function_exists('getWidgetContent')) {
    function getWidgetContent($className)
    {
        $name = toWidgetName($className);

        $w = new App\Models\Admin\Widgets();
        $result = $w->where('name', '=', $name)->first();

        return $result->content;
    }
}

// OPTIONS
if (!function_exists('option')) {
    function option($name)
    {
        if (!strpos($name, '.')) {
            $arr = explode('.', $name);
            $result = App\Models\Admin\Options::where('name', 'like', "%$arr[0].%")->get();//)->value;
            return convertModelObjectToArray($result);//$result[0]->toArray();
        } else
            return (App\Models\Admin\Options::where('name', '=', $name)->first())->value;
    }
}

// SIDEBAR
if (!function_exists('getSidebar')) {
    function getSidebar($sbName)
    {
        return view("components.sidebars.$sbName", ['sidebars' => getSidebarData($sbName)]);
    }
}

if (!function_exists('getSidebarData')) {
    function getSidebarData($sidebarName)
    {
        switch ($sidebarName) {
            default:
                return [];
                break;
        }
    }
}

// SLIDER
if (!function_exists("getSliders")) {
    function getSliders($fields)
    {
        $sliders = (new App\Models\Admin\Sliders())->select($fields)->get();
        return $sliders->toArray();
    }
}

if (!function_exists("slider")) {
    function slider($name)
    {
        $slides = DB::table("imus_slide")
            ->select(["id", "name", "src", "order", "post_id"])
            ->where(
                "group",
                DB::raw("(select id from `imus_slider` where `name` = '$name')")
            )
            ->orderBy("order")
            ->get();
        return $slides->toArray();
    }
}

// CATEGORIES
if (!function_exists("getCategories")) {
    function getCategories($fields)
    {
        $cats = (new \TCG\Voyager\Models\Category())->select($fields)->get();
        return $cats;
    }
}

if (!function_exists("getCatsById")) {
    // return category format tree
    function getCatsById($catId)
    {
        //$cat = getCategories(["id", "parent_id", "name", "slug"]);
        return (new \TCG\Voyager\Models\Category())->find($catId);
    }
}

if (!function_exists("getCatsTree")) {
    // return category format tree
    function getCatsTree($childId)
    {
        $cat = getCatsById($childId);
        if ($cat->parent_id != null) {
            $pId = $cat->parent_id;
            $cat->url = route("imus.post.category", $cat->slug);
            $cat->parent_id = getCatsTree($pId);
        }
        return $cat->toArray();
    }
}

// SITE PREFIX
if (!function_exists("getTitle")) {
    function getTitle($name)
    {
        return $name . " | " . setting('site.title');
    }
}

// PARTNER
if (!function_exists("getPartners")) {
    function getPartners()
    {
        return DB::table("imus_partner")
            ->get();
    }
}

// HOME
if (!function_exists("isHome")) {
    function isHome()
    {
        $route = Route::currentRouteName();
        return $route == "home";
    }
}

// POST
if (!function_exists("getPostBy")) {
    function getPostBy($by, $args = [], $pagi = 5)
    {
        return \TCG\Voyager\Models\Post::select($args)
            ->where($by->key, $by->value)
            ->paginate($pagi);
    }
}

if (!function_exists("author")) {
    function author($id)
    {
        return \TCG\Voyager\Models\User::select(["id", "name"])
            ->where("id", $id)
            ->first();
    }
}

if (!function_exists("getGalleryGroup")) {
    function getGalleryGroup()
    {
        $query = \Illuminate\Support\Facades\DB::table("imus_galleries")
            ->distinct();
//            ->get();

        $groups = $query->addSelect("group")->get();
        return $groups;
    }
}

if (!function_exists("gallery")) {
    function galleries($fields = "*", $group = null)
    {
        if ($group == null) {
            return \App\Models\Galleries::select($fields)->get();
        }
        return \App\Models\Galleries::select($fields)->where("group", $group)->get();
    }
}

if (!function_exists("students_filter")) {
    function students_filter($key)
    {
        return DB::table('imus_students')->select($key)->distinct()->get();
    }
}
