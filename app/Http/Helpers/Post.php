<?php
/**
 * @author kogoro
 * @created_at 8/31/19
 **/

use TCG\Voyager\Models\Post as MPost;
use TCG\Voyager\Models\Category as MCat;
use Illuminate\Support\Facades\DB;

class Post
{
    public static function getPostById($id)
    {
        return MPost::find($id);
    }

    public static function getPostByCat($slug, $limit = 6)
    {
        $posts = DB::table("posts")
            ->leftJoin("categories", "posts.category_id", "=", "categories.id")
            ->select(["posts.*", "categories.name"])
            ->where(
                "posts.category_id",
                DB::raw("(select `id` from `categories` where `slug` = '$slug')")
            )
            ->limit($limit)
            ->orderBy("created_at")
            ->get();

        return $posts;
    }

    public static function featurePosts()
    {
        return (new MPost())->where("featured", "1")
            ->limit(5)
            ->orderBy("created_at")
            ->get();
    }
}
