<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Page;

class PageController extends Controller
{
    //
    protected $pageModel;

    public function __construct()
    {
        $this->pageModel = new Page();
    }

    public function show($slug)
    {
        $page = $this->pageModel->where("slug","/zz/", $slug)->first();

        return $page ? view("pages.single", compact('page')) : abort("404");
    }

    public function contact()
    {
        return view("frontend.contact");
    }
    public function about()
    {
        return view("frontend.about");
    }    
    public function blog()
    {
        return view("frontend.blog");
    }   
     public function singleBlog()
    {
        return view("frontend.single-blog");
    }
}
