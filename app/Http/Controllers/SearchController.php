<?php

/**
 * @author: Nong Van Du
 */

namespace App\Http\Controllers;

use App\Models\Admin\Student;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    function search(Request $request)
    {
        $full_name = $request->has('full_name') ? $request->get('full_name') : '';
        $query = $this->prepare_query($request);
        $students = Student::where($query)->get();
        return view('pages.search', compact('students', 'full_name'));
    }

    function search_ajax(Request $request)
    {
        $query = $this->prepare_query($request);
        $students = Student::where($query)->get();
        return view('components.search.student_table', compact('students'));
    }

    private function prepare_query($request)
    {
        $query = [];
        $params = $request->all();
        unset($params['_token']);
        foreach ($params as $key=>$value) {
            $arr = [];
            $arr[] = $key;
            $arr[] = 'LIKE';
            $arr[] = '%'. $value . '%';
            $query[] = $arr;
        }

        return $query;
    }
}
