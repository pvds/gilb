<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Options;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerSettingsController;

class OptionController extends VoyagerSettingsController
{
    private $opt;

    public function __construct()
    {
        $this->opt = new Options();
    }

    /**
     * @Overwrite
     *
     * @return admin-setting view
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('browse', $this->opt);

        $data = $this->opt->orderBy('id', 'DSC')->get();

        $settings = [];
        $active = "";
        foreach ($data as $d) {
            if ($active == "")
                $active = $d->group;
            $settings[$d->group][] = $d;
        }

        $groups_data = $this->opt->select('group')->distinct()->get();
        $groups = [];
        foreach ($groups_data as $group) {
            if ($group->group != '') {
                $groups[] = $group->group;
            }
        }

        return Voyager::view('voyager::options.index', compact('settings', 'groups', 'active'));
    }

    /**
     * @Overwrite
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|VoyagerSettingsController
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request)
    {
        // Check permission
        $this->authorize('edit', $this->opt);
        $reqs = $request->all();

//        dd($request->all());

        $options = $this->opt->all();

        $excludes = ['_token', 'group', '_method'];
        $result = false;
        foreach ($reqs as $key => $value) {
            if (!empty($value) && !in_array($key, $excludes)) {
                $_g = Str::slug($request->get('group'));
                $tempKey = $_g . "." . substr($key, strlen($_g) + 1, strlen($key));
                $content = $this->getContentBasedOnType($request, 'settings', (object)[
                    'type' => $this->getType($options, $tempKey),
                    'field' => $key,
                    'details' => "",
                    'group' => $request->get('group'),
                ]);

                $key = $_g . "." . substr($key, strlen($_g) + 1, strlen($key));
                $result = $this->opt->where('name', '=', $key)->update(['value' => $content]);
            }
        }

        return $result ? back()->with([
            'message' => __('voyager::settings.successfully_saved'),
            'alert-type' => 'success',
        ]) : back()->with([
            'message' => __('voyager::settings.error_saved'),
            'alert-type' => 'success',
        ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|VoyagerSettingsController
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        // Check permission
        $this->authorize('add', $this->opt);


        foreach ($request->all() as $key => $value) {
            if ($key != "_token") {
                if ($key == "name")
                    $value = Str::slug($request->get('group')) . "." . $value;
                $this->opt->{$key} = $value;
            }
        }
        $this->opt->save();

        return back()->with([
            'message' => __('voyager::settings.successfully_created'),
            'alert-type' => 'success',
        ]);
    }


    /**
     * @param $request
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function remove(Request $request)
    {
        // Check permission
        $this->authorize('delete', $this->opt);

        $id = $request->get('id');

        $this->opt->destroy($id);

        return back()->with([
            'message' => __('voyager::settings.successfully_removed'),
            'alert-type' => 'success',
        ]);
    }

    protected function getType($options, $key)
    {
        foreach ($options as $_opt => $value) {
            if ($key == $value->name)
                return $value->type;
        }
    }
}
