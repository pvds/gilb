<?php

namespace App\Http\Controllers\Admin;

use App\Excels\StudentsImport;
use App\Models\Admin\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class StudentController extends VoyagerBaseController
{
    public function prepare_import(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route("voyager.students.index")
                ->with([
                    'message' => "Vui lòng chọn file trước khi thực hiện",
                    'alert-type' => 'error',
                ]);
        }

        if ($request->hasFile('import_file')) {
            $file = $request->import_file;

            if ($this->checkFormatExcel($file)) {
                if ($file->getClientOriginalExtension() == 'xls') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $spreadsheet = $reader->load($file);
                $xls_data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                $start_row = 6;
                $phones_failed = [];
                $emails_failed = [];

                for ($i = $start_row; $i <= count($xls_data); $i++) {
                    $row = $xls_data[$i];
                    if (is_null($row['A']))
                        continue;

                    $student_phone = Student::where('phone', $row['C'])->first();
                    if (!is_null($student_phone))
                        $phones_failed[] = $row['C'];

                    $student_email = Student::where('email', $row['D'])->first();
                    if (!is_null($student_email))
                        $emails_failed[] = $row['D'];
                }

                try {
                    Excel::import(new StudentsImport, $file);
                } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                    return redirect()
                        ->route("voyager.students.index")
                        ->with([
                            'message' => "Nhập thất bại. Vui lòng kiểm tra lại.",
                            'alert-type' => 'error',
                        ]);
                }

                if (count($phones_failed) > 0 || count($emails_failed) > 0) {
                    $msg = "Cảnh báo.";

                    if (count($phones_failed) > 0) {
                        $msg .= "<br />Có " . count($phones_failed) . " SĐT đã tồn tại.<br />" . implode(', ', $phones_failed) . ".";
                    }

                    if (count($emails_failed) > 0) {
                        $msg .= "<br />Có " . count($emails_failed) . " Email đã tồn tại.<br />" . implode(', ', $emails_failed) . ".";
                    }

                    $msg .= "<br />Vui lòng kiểm tra lại dữ liệu trước khi nhập.";

                    return redirect()
                        ->route("voyager.students.index")
                        ->with([
                            'message' => $msg,
                            'alert-type' => 'warning',
                        ]);
                } else {
                    return redirect()
                        ->route("voyager.students.index")
                        ->with([
                            'message' => "Nhập thành công",
                            'alert-type' => 'success',
                        ]);
                }
            }
        }

        return null;
    }

    private function checkFormatExcel($file)
    {
        $allow_format = array('xls', 'xlsx');
        $file_ext = $file->getClientOriginalExtension();
        if (!in_array($file_ext, $allow_format))
            return false;
        return true;
    }
}
