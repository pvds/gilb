<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Widgets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Http\Interfaces\ControllerInterface as ControllerInterfaceAlias;

class WidgetController extends Controller implements ControllerInterfaceAlias
{
    protected $w;

    public function __construct()
    {
        $this->w = new Widgets();
    }

    public function index()
    {
        $this->authorize('browse', $this->w);
        return view("voyager::widgets.index")->with([
            'widgets' => Widgets::all(),
            'shortcodes' => [
                'recent_post' => [
                    'code' => 'recent_post limit=5 layout=default',
                    'name' => "Recent post",
                    'img' => "https://imus.vn/public/storage/galleries/April2019/4QUvYoNnKa2yWCP2F1lq.jpg"
                ],
                'categories' => [
                    'code' => 'categories layout=default',
                    'name' => "Category list",
                    'img' => "https://imus.vn/public/storage/galleries/April2019/4QUvYoNnKa2yWCP2F1lq.jpg"
                ],
            ]
        ])->withoutShortcodes();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function add(Request $request)
    {
        // check permission
        $this->authorize('add', $this->w);

        if ($this->isExist([['name', '=', $request->get('name')]], $this->w))
            return back()->with([
                'message' => "Widget has existed",
                'alert-type' => 'error',
            ]);

        foreach ($request->all() as $key => $value) {
            if ($key !== "_token") {
                $this->w->{$key} = $value;
            }
        }

        $this->w->save();
        Artisan::call('make:widget --plain ' . $request->get('name'));

        //dd($request->all());
        return back()->with([
            'message' => __('voyager::settings.successfully_created'),
            'alert-type' => 'success',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request)
    {
        $this->authorize('edit', $this->w);
        $result = $this->w->where('id', '=', $request->get('_id'))->update(['content' => $request->get('content')]);
        return back()->with([
            'message' => __('voyager::settings.successfully_saved'),
            'alert-type' => 'success',
        ]);
    }

    public function remove(Request $request)
    {
        $this->authorize('delete', $this->w);
        $id = $request->get('id');
        $this->w->destroy($id);

        // remove file in App/Widgets

        return back()->with([
            'message' => __('voyager::settings.successfully_deleted'),
            'alert-type' => 'success',
        ]);
    }

    /**
     * @param $args
     * @param $obj
     * @return bool true if not empty
     */
    public function isExist($args, $obj)
    {
        $result = $obj->where($args)->get();
        return count($result) > 0 ? true : false;
    }
}
