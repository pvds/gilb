<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HttpController extends Controller
{
    public function __construct()
    {
    }

    public function handle(Request $request)
    {
//        return $request->all();
        $func = $request->get("method");
        return call_user_func_array([$this, $func], ["data" => $request->get('d')]);
    }

    public function get($param)
    {
        $k = $param['keyword'];
        $col = $param['col'];
        $type = $param['type'];
        $data = DB::table($param["table"])
            ->where($col, "like", "%$k%")
            ->get();

        if (count($data->toArray()) == 0)
            return false;

        switch ($type) {
            case "selection":
                return view("components.elements.$type", compact('data', 'col'))->with(["param" => $param]);
                break;
            default:
                return "x";
                break;
        }
    }
}
