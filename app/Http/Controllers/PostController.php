<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Post;

class PostController extends Controller
{
    public function __construct()
    {
    }

    public function show($slug)
    {
        $post = Post::where("slug", $slug)->first();

        if ($post == null)
            abort(404);
        $args = [
            "catId" => $post->category_id
        ];
        return view("pages.news", compact("post"))->with($args)->withShortcodes();
    }

    public function category($slug)
    {
        $cat = Category::where("slug", $slug)->first();
        $posts = getPostBy((object)[
            "key" => "category_id",
            "value" => $cat->id
        ], ["*"], 7);

        $args = [
            'cat' => $cat,
            'catId' => $cat->id
        ];
        return view("pages.archived", compact("posts"))->with($args)->withShortcodes();
    }
}
