<?php
/**
 * @author kogoro
 * @created_at 8/31/19
 **/

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class MenusProvider
{
    protected $menus = [];

    public function __construct()
    {
        $names = ["primary", "footer"];
        foreach ($names as $item) {
            $this->menus["$item"] = menu("$item", "_json");
//            array_push($this->menus, menu("$item"));
        }
    }

    public function compose(View $view)
    {
        return $view->with("menus", $this->menus);
    }
}
