<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/

namespace App\Http\Interfaces;

interface ControllerInterface
{
    /**
     * @param $args
     * @param $obj
     * check if has value in database
     */
    public function isExist($args, $obj);
}

