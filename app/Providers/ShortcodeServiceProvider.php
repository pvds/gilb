<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Shortcode;

class ShortcodeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Shortcode::register('recent_post', "App\Shortcodes\BaseShortcode@recentPost");
        Shortcode::register('categories', "App\Shortcodes\BaseShortcode@categories");
        Shortcode::register('poll', "App\Shortcodes\BaseShortcode@poll");
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
//        Shortcode::enable();
    }
}
