<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * @require all .php file in app/Helper
         */
        foreach (glob(app_path() . '/Http/Helpers/*.php') as $file) {
            require_once($file);
        }
    }
}
