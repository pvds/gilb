<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\View;

class HarlayDavidson extends AbstractWidget
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->config['content'] = getWidgetContent(get_class($this));
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        return View::make('widgets.base', [
            'config' => $this->config,
        ])->with('content', $this->config['content']);
    }
}
