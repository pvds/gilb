<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
    protected $table = "imus_partner";
}
