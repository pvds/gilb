<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $table = "imus_options";
}
