<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Widgets extends Model
{
    protected $table = "imus_widgets";
}
