<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'imus_students';
    protected $fillable = [
        'full_name',
        'phone',
        'email',
        'year',
        'educational',
        'course',
        'class',
        'association',
        'bll',
        'position',
        'work',
        'address'
    ];
}
