<?php

namespace App\Excels;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Admin\Student;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class  StudentsImport implements ToModel, WithValidation, WithStartRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model|null
     */

    public function model(array $row)
    {
        if (!isset($row[0])) {
            return null;
        }

        $student_phone = Student::where('phone', $row['2'])->first();
        if (!is_null($student_phone))
            return null;

        $student_email = Student::where('email', $row['3'])->first();
        if (!is_null($student_email))
            return null;

        return new Student([
            'full_name' => $row[1],
            'phone' => $row[2],
            'email' => $row[3],
            'year' => $row[4],
            'educational' => $row[5],
            'course' => $row[6],
            'class' => $row[7],
            'association' => $row[8],
            'bll' => $row[9],
            'position' => $row[10],
            'work' => $row[11],
            'address' => $row[12],
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 6;
    }
}
