<?php

namespace App\Export;

use App\Models\Admin\Student;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;


class StudentsExport implements FromCollection
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Student::all(array(
            'full_name',
            'phone',
            'email',
            'year',
            'educational',
            'course',
            'class',
            'association',
            'bll',
            'position',
            'work',
            'address'
        ));
    }

    public function export()
    {
        return Excel::download(new StudentsExport(), 'danh-sach-cuu-sinh-vien-co-dien.xlsx');
    }
}
