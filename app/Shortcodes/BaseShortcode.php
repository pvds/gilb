<?php
/**
 * @author kogoro
 * @created_at 7/26/19
 **/

namespace App\Shortcodes;

use App\Repositories\Post\PostRepository;
use App\Repositories\PostsCategory\PostsCategoryRepository;
use App\Repositories\PostsCategoryRelation\PostsCategoryRelationRepository;
use Illuminate\Support\Facades\View;

class BaseShortcode
{
    /**
     * @param $shortcode
     * @param $content
     * @param $compiler
     * @param $name
     * @param $viewData
     * @return \Illuminate\Contracts\View\View
     */


    public function __construct()
    {
    }

    public function recentPost($shortcode, $content, $compiler, $name, $viewData)
    {
//        $s = $shortcode->toArray();
//        $limit = isset($s['limit']) ? $s['limit'] : 5;
//        $layout = isset($s['layout']) ? $s['layout'] : 'default';
//        $posts = $this->postRepo->getNewPosts($limit);
//        return view("shortcodes.recent_post", compact('posts', 'layout'));
        return "I am 007";
    }

    public function categories($shortcode, $content, $compiler, $name, $viewData)
    {
//        $categories = $this->postRepo->getTree();
//        return view("shortcodes.categories", compact('categories'));
    }

    public function poll($shortcode, $content, $compiler, $name, $viewData)
    {
        return view("shortcodes.poll", ['id' => is_numeric($shortcode->id) ? (int)$shortcode->id : 1]);
    }
}
