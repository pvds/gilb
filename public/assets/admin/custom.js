function slug(Text) {
    Text = vietnames(Text);
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g, '')
        .replace(/ +/g, '-');
}

function vietnames(slug) {
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    return slug;
}


$(document).ready(function () {
    $("input.iFaker").keyup(function (e) {
        let target = e.target.getAttribute("data-target");
        let data = {
            _token: csrfToken,
            method: "get",
            d: {
                keyword: $(this).val(),
                table: e.target.getAttribute("data-fake"),
                col: e.target.getAttribute("data-col"),
                type: e.target.getAttribute("data-ftype")
            }
        };
        let parent = $(this).parent();
        $.ajax({
            url: httpUrl,
            type: "post",
            data: data,
            success: function (res) {
                let id = `#${data.d.table}_${data.d.type}`;
                let child = parent.find(id);
                if (child) {
                    child.remove();
                }
                if (res !== false) {
                    parent.append(res);
                    $(id).change(function (e) {
                        let value = e.target.value;
                        let text = $(e.target).children("option:selected").html();
                        $("input.iFaker").val(text);

                        $(`input[name=${target}]`).val(parseInt(value));
                    })
                }
            }
        });
    });
});
