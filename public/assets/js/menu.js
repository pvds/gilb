// menu
$(document).ready(function () {
    $('body').toggleClass('TouchMobile');
    $(".dropdown").hover(
        function () {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("350");
            $(this).toggleClass('open');
        },
        function () {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("350");
            $(this).toggleClass('open');
        }
    );
});
//end  menu
// add active menu
$(document).ready(function () {
    var url = window.location;
    $('nav ul li a[href="' + url + '"]').parent().addClass('active');
    $('nav ul li a').filter(function () {
        return this.href == url;
    }).addClass('active').parent().addClass('active').parent().parent().addClass('active');
});
// end add active menu
// add active tab cau hoi
$(document).ready(function () {
    $(".tab-cauhoi a").click(function () {
        $(".tab-cauhoi a").removeClass("new-active");
        $(this).addClass("new-active");
    });
});
// endadd active
$(document).ready(function () {
    $(".click-chat").click(function () {
        $(".position-chat").removeClass("resize");
        $(".position-chat").addClass("active");
        // $(this).addClass("new-active");
    });
    $(".btn-chat .btn-show").click(function () {
        $(".position-chat").removeClass("active");
        // $(this).addClass("new-active");
    });
    $(".btn-chat .btn-resize").click(function () {
        $(".position-chat").toggleClass("resize");
        $(this).addClass("btn-active");
    });


});
/*về đầu trang*/
jQuery(document).ready(function ($) {
    if ($(".btn-top").length > 0) {
        // $(window).scroll(function () {
        // 	var e = $(window).scrollTop();
        // 	if (e > 300) {
        // 		$(".btn-top").show()
        // 	} else {
        // 		$(".btn-top").hide()
        // 	}
        // });
        $(".btn-top").click(function () {
            $('#s4-workspace').animate({
                scrollTop: 0
            })
        })
    }
});
/*cong cu*/
$(document).ready(function () {
    $(".congcu ul li h4").click(function () {
        $(".congcu ul li ul").toggleClass('block');

    });
});
/*end cong cu*/


// menu mobile 
$(document).ready(function () {
    $menuLeft = $('.pushmenu-left');
    $nav_list = $('#nav_list');
    $nav_list1 = $('#nav_list1');

    $nav_list.click(function () {
        var classActive = $menuLeft.attr('class');
        var classact = 'active';
        if (classActive.indexOf('pushmenu-open') > 0) {
            $('body').addClass('TouchMobile');
           $nav_list1.removeClass('btn-check');
        }
        else {
            $('body').removeClass('TouchMobile');
			 $nav_list1.addClass('btn-check');
            
        }
        $(this).toggleClass(classact);
        $('.pushmenu-push').toggleClass('pushmenu-push-toright');
        $menuLeft.toggleClass('pushmenu-open');
    });
});


// dao tao
$(document).ready(function () {
    $(function () {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', { el: this.el, multiple: this.multiple }, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
			$next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            };
        }

        var accordion = new Accordion($('.accordion'), false);
    });

});
// end dao tao
// pop up tin chi tiet
// $(document).ready(function() {
// $('body').append('<div class="product-image-overlay"><span class="product-image-overlay-close">x</span><img src="" /></div>');
// var productImage = $('.img-class img');
// var productOverlay = $('.product-image-overlay');
// var productOverlayImage = $('.product-image-overlay img');

// productImage.click(function () {
// var productImageSource = $(this).attr('src');

// productOverlayImage.attr('src', productImageSource);
// productOverlay.fadeIn(100);
// $('body').css('overflow', 'hidden');

// $('.product-image-overlay-close').click(function () {
// productOverlay.fadeOut(100);
// $('body').css('overflow', 'auto');
// });
// });
// });

// popup img tin chi tiet
$(document).ready(function () {

    // dem so luong anh
    var dem = $(".img-class img").length;

    //lấy stt cua img
    // $('.img-class .img-frame').children('img').click(function() {
    // var index1 = $(this).index() + 1;


    // event click
    $(".img-class img").on("click", function () { lightbox(); });
});
//search
$(document).ready(function() {
	$("#search-click").click(function(){
		$(".ul-form-search").toggleClass("active");
		
	});
});
