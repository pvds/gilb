$.sidebarMenu = function(menu) {
  var animationSpeed = 300;
  
  $(menu).on('click', 'li a', function(e) {
    var $this = $(this);
    var checkElement = $this.next();

    if (checkElement.is('.treeview-menu') && checkElement.is(':visible')) {
      checkElement.slideUp(animationSpeed, function() {
        checkElement.removeClass('menu-open');
      });
      checkElement.parent("li").removeClass("active");
    }

    //If the menu is not visible
    else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
      //Get the parent menu
      var parent = $this.parents('ul').first();
      //Close all open menus within the parent
      var ul = parent.find('ul:visible').slideUp(animationSpeed);
      //Remove the menu-open class from the parent
      ul.removeClass('menu-open');
      //Get the parent li
      var parent_li = $this.parent("li");

      //Open the target menu and add the menu-open class
      checkElement.slideDown(animationSpeed, function() {
        //Add the class active to the parent li
        checkElement.addClass('menu-open');
        parent.find('li.active').removeClass('active');
        parent_li.addClass('active');
      });
    }
    //if this isn't a link, prevent the page from being redirected
    if (checkElement.is('.treeview-menu')) {
      e.preventDefault();
    }
  });
}
// menu
 $(document).ready(function(){
	  var widthWinfirst = window.innerWidth;
     if (widthWinfirst > 1060){
		$(".treeview").hover(            
        function() {
            $('.treeview-menu', this).not('.in .treeview-menu').stop(true,true).css("display","block");
            $(this).toggleClass('open');        
        },
        function() {
            $('.treeview-menu', this).not('.in .treeview-menu').stop(true,true).css("display","none");
            $(this).toggleClass('open');       
        }
    );
    }	
     $.sidebarMenu($('.sidebar-menu'));
});
 //end  menu
 // add active menu
 // add active menu
$(document).ready(function () {
     var url = window.location;  
      $('.sidebar-menu li a[href="' + url + '"]').parent().addClass('active');
       $('.sidebar-menu li a').filter(function () {
          return this.href == url;		
     }).addClass('active').parent().addClass('active').parent().parent().addClass('active').parent().addClass('menu-open').parent().addClass('active');
  });
// end add active menu


