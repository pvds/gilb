<?php

/*
 * This file is part of Flarum.
 *
 * (c) Toby Zerner <toby.zerner@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Meesudzu\TNUT\Login;

use Exception;
use Flarum\Forum\Auth\Registration;
use Flarum\Forum\Auth\ResponseFactory;
use Flarum\Http\UrlGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\RedirectResponse;

class TNUTAuthController implements RequestHandlerInterface
{
    /**
     * @var ResponseFactory
     */
    protected $response;

    /**
     * @var UrlGenerator
     */
    protected $url;

    /**
     * @param ResponseFactory $response
     * @param UrlGenerator $url
     */
    public function __construct(ResponseFactory $response, UrlGenerator $url)
    {
        $this->response = $response;
        $this->url = $url;
    }

    /**
     * @param Request $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function handle(Request $request): ResponseInterface
    {
        $redirectUri = $this->url->to('forum')->route('auth.tnut');

        $queryParams = $request->getQueryParams();

        $code = array_get($queryParams, 'code');

        if (! $code) {
            $authUrl = "http://tnut.local/auth/other?redirect=".$redirectUri;
            return new RedirectResponse($authUrl);
        }

        $username = array_get($queryParams, 'username');
        $email = array_get($queryParams, 'email');
        $avatar = array_get($queryParams, 'avatar_url');

        return $this->response->make(
            'tnut', '69',
            function (Registration $registration) use ($username, $email, $avatar){
                $registration
                    ->provideTrustedEmail($email)
                    ->provideAvatar($avatar)
                    ->suggestUsername($username)
                    ->setPayload(['username' => $username, 'email' => $email, 'avatar_url' => $avatar]);
            }
        );
    }
}
