# tnut-login

![License](https://img.shields.io/badge/license-MIT-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/meesudzu/laravel-login.svg)](https://packagist.org/packages/meesudzu/laravel-login)

A [Flarum](http://flarum.org) extension.

### Installation

Use [Bazaar](https://discuss.flarum.org/d/5151-flagrow-bazaar-the-extension-marketplace) or install manually with composer:

```sh
composer require meesudzu/tnut-login
```

### Updating

```sh
composer update meesudzu/tnut-login
```

### Links

- [Packagist](https://packagist.org/packages/meesudzu/tnut-login)
