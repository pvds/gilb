import {extend} from 'flarum/extend';
import app from 'flarum/app';
import LogInButtons from 'flarum/components/LogInButtons';
import LogInButton from 'flarum/components/LogInButton';

app.initializers.add('meesudzu/tnut-login', () => {
  extend(LogInButtons.prototype, 'items', function (items) {
    items.add('tnut',
      <LogInButton
        className="Button LogInButton--tnut"
        icon="fab fa-laravel"
        path="/auth/tnut">
        Đăng Nhập Qua TNUT
      </LogInButton>
    );
  });
});
