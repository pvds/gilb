<?php return array(
    'debug' => true,
    'database' =>
        array(
            'driver' => 'mysql',
            'host' => 'localhost',
            'port' => 3306,
            'database' => 'imus_cuusv_tnut',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => 'forums_',
            'strict' => false,
            'engine' => 'InnoDB',
            'prefix_indexes' => true,
        ),
    'url' => 'http://cuusv-tnut.nta.net/forum',
    'paths' =>
        array(
            'api' => 'api',
            'admin' => 'admin',
        ),
);
