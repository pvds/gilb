<?php return array(
    'debug' => true,
    'database' =>
        array(
            'driver' => 'mysql',
            'host' => 'localhost',
            'port' => 3306,
            'database' => 'imusdev_cuusvcn',
            'username' => 'imusdev_admin',
            'password' => 'r6KnZEQrWA',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => 'forums_',
            'strict' => false,
            'engine' => 'InnoDB',
            'prefix_indexes' => true,
        ),
    'url' => 'https://cuusv-tnut.imus.vn/forum',
    'paths' =>
        array(
            'api' => 'api',
            'admin' => 'admin',
        ),
);
