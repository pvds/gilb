<?php

/*
 * This file is part of meesudzu/laravel-login.
 *
 * Copyright (c) 2019 meesudzu.
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Meesudzu\TNUT\Login;
use Flarum\Extend;

return [
    (new Extend\Frontend('forum'))
        ->js(__DIR__.'/js/dist/forum.js')
        ->css(__DIR__.'/less/forum.less'),

    (new Extend\Routes('forum'))
        ->get('/auth/tnut', 'auth.tnut', TNUTAuthController::class),
];
