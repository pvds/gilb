{!! array_get($forum, 'headerHtml') !!}

<link href="/public/assets/css/main.css" rel="stylesheet">
<link href="/public/assets/css/sidebar-menu.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
      integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<div id="app" class="App">

    <div id="app-navigation" class="App-navigation"></div>

    <div id="drawer" class="App-drawer">
        <div class="none-padding" id="new_head">
            <div class="none-padding">
                <!-- head -->
                <div>
                    <!-- banner top -->
                    <div class="none-padding">
                        <div class="col-xs-12 none-padding banner-top">
                            <a href="javascript:;" class="banner">
                                <img src="/public/images/sv_banner.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end banner -->

            <!-- menu -->
            <div class="col-xs-12 none-padding">
                <div class="container none-padding menu-bg">
                    <div class="none-padding">
                        <div class="none-padding banner-right ">
                            <div class="container none-padding">
                                <div class="pushmenu-push ">
                                    <div class="pushmenu pushmenu-left menu-open">
                                        <section class="ul-width ">
                                            <ul class="sidebar-menu">
                                                <li class="treeview">
                                                    <a href="/" class="">
                                                        <img src="/public/images/ic_home.png"
                                                             class="icon-home">
                                                    </a>
                                                </li> 
                                                <li class="treeview">
                                                    <a target="_self"
                                                       href="/zz/tin-tuc/danh-muc/cong-van-thong-bao">
                                                        <span>Công văn thông báo</span>
                                                    </a>
                                                </li>
                                                <li class="treeview">
                                                    <a target="_self" href="/zz//tin-tuc/danh-muc/tin-tuc-su-kien">
                                                        <span>Tin tức sự kiện</span>
                                                    </a>
                                                </li>
                                                <li class="treeview">
                                                    <a target="_self" href="/zz/tin-tuc/danh-muc/gioi-thieu">
                                                        <span>Giới thiệu</span>
                                                    </a>
                                                </li>

                                                <li class="treeview">
                                                    <a target="_self" href="/zz/forum">
                                                        <span>Diễn Đàn</span>
                                                    </a>
                                                </li>
                                                <div id="header-primary" class="Header-primary"></div>
                                                <div id="header-secondary" class="Header-secondary"></div>
                                            </ul>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <!--home-mobile  -->
                            <div class="col-xs-6 none-padding icon-menu" id="nav_list">
                    <span id="nav_list1" class="active">

					</span>
                            </div>
                            <!--  -->
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- head end-->

        <header id="header" class="App-header" style="background: none">
            <div id="header-navigation" class="Header-navigation" style="display: none"></div>
            <div class="container">
                {{--                <h1 class="Header-title">--}}
                {{--                    <a href="{{ array_get($forum, 'baseUrl') }}" id="home-link">--}}
                {{--                        @if ($logo = array_get($forum, 'logoUrl'))--}}
                {{--                            <img src="{{ $logo }}" alt="{{ array_get($forum, 'title') }}" class="Header-logo">--}}
                {{--                        @else--}}
                {{--                            {{ array_get($forum, 'title') }}--}}
                {{--                        @endif--}}
                {{--                    </a>--}}
                {{--                </h1>--}}
                {{--                <div id="header-primary" class="Header-primary"></div>--}}
                {{--                <div id="header-secondary" class="Header-secondary"></div>--}}
            </div>
        </header>

    </div>

    <main class="App-content">
        <div id="content"></div>

        {!! $content !!}

        <div class="App-composer">
            <div class="container">
                <div id="composer"></div>
            </div>
        </div>
    </main>

</div>

{{--<footer>--}}
{{--    <div class="">--}}
{{--        <div class="bg-footer" style="position: relative">--}}

{{--            <div class="container none-padding ">--}}
{{--                <div class="footer">--}}
{{--                    <div class="row" style="margin: 0">--}}
{{--                        <div class="col-md-4">--}}
{{--                            <h3 class="tieude">Giới thiệu</h3>--}}
{{--                            <div class="content">--}}
{{--                                <p><strong>Hội cựu sinh vi&ecirc;n &ndash; Trường Đại học Kỹ thuật C&ocirc;ng nghiệp Th&aacute;i Nguy&ecirc;n</strong></p>--}}
{{--                                <p>Địa chỉ: Đường 3-2, T&iacute;ch Lương, TP Th&aacute;i Nguy&ecirc;n<br />Tel: (84)2803847145, Fax (84)280384740</p>--}}
{{--                                <p>Website: http://www.tnut.edu.vn - Email: office@tnut.edu.vn</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <h3 class="tieude">liên kết web</h3>--}}
{{--                            <div class="content">--}}
{{--                                <div class="list">--}}
{{--                                    <ul class="ul-footer">--}}
{{--                                        <li>Trang chủ</li>--}}
{{--                                        <li>Công văn thông báo</li>--}}
{{--                                        <li>Tin tức sự kiện</li>--}}
{{--                                        <li>Giới thiệu</li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-4">--}}
{{--                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fimus.jsc%2F&tabs=timeline&width=340&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=441147446645129" width="340" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </div>--}}
{{--</footer>--}}

{!! array_get($forum, 'footerHtml') !!}
